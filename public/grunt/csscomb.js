module.exports = function () {
  "use strict";

  return {
    options: {
      config: '<%= config.source.sass %>/.csscomb.json'
    },
    css: {
      expand: true,
      cwd: '<%= config.destination.css %>',
      src: ['**/*.css', '!**/*.min.css'],
      dest: '<%= config.destination.css %>/'
    },
  };
};
