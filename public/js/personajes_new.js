$(document).ready(function() {
    $(window).one('siteReady', function() {
        $('.selectAjax').each(function() {
            $(this).select2({
                placeholder: {
                    id: '-1',
                    text: 'Buscar...'
                },
                ajax: {
                    url: "/api/search/" + $(this).data('model'),
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        return {
                            q: params.term, // search term
                            page: params.page
                        };
                    },
                    cache: false
                },
                escapeMarkup: function(markup) { return markup; }, // let our custom formatter work
                minimumInputLength: 3
            });
        });

        $('.selectAjax').parent().find('label').append(' <span class="fa fa-times-circle clear-select"></span>');

        $('.clear-select').click(function() {
            $(this).prev('select').val('').trigger('change');
        });

        $('input.check-open').on('change', function() {
            if ($(this).is(':checked')) {
                $(this).parent().next('.row').slideDown();
                if ($(this).data('toggle')) $($(this).data('toggle')).attr('disabled', 'disabled');
            } else {
                $(this).parent().next('.row').slideUp();
                if ($(this).data('toggle')) $($(this).data('toggle')).removeAttr('disabled');
            }
        });

        var $uploadCrop = null;

        function readFile(input) {
 			if (input.files && input.files[0]) {
	            var reader = new FileReader();
	            
	            reader.onload = function (e) {
					$('#upload-editor, #upload-controls').slideDown();
	            	$uploadCrop.bind({
	            		url: e.target.result
	            	}).then(function(){
	            		console.log('jQuery bind complete');
	            	});
	            	
	            }
	            
	            reader.readAsDataURL(input.files[0]);
                $('#ext').val(input.files[0].name.split('.').pop().toLowerCase());
	        }
	        else {
		        alert("Navegador no soporta la edición de imagenes.");
		    }
		}

        $('input[name="pic"]').on('change', function () { readFile(this); });

        $uploadCrop = new Croppie(document.querySelector('#upload-editor'), {
            enableExif: true,
            enableOrientation: true,
            viewport: {
                width: 300,
                height: 300,
                type: 'square'
            },
            boundary: {
                width: 300,
                height: 300
            }
        });

        $('.rotar').on('click', function(ev) {
			$uploadCrop.rotate(parseInt($(this).data('deg')));
		});

        $('.agregarPotencial').click(function() {
            var templateHtml = $($('#potencialTemplate').html().replace(/(\{tipo\})/ig, $(this).data('tipo')));
            $(this).prev().append(templateHtml);
            bindMultiSelect(templateHtml);
        })

        $('.agregarNivel').click(function() {
            $(this).prev().append($($('#nivelTemplate').html()));
            bind();
        });

        $('.agregarOferta').click(function() {
            $(this).prev().append($($('#ofertaTemplate').html()));
            $('.add-select2').removeClass('.add-select2').select2();
            $('.add-datepicker').removeClass('.add-datepicker').datepicker();
            bind();
        });

        $('.agregarProyecto').click(function() {
            $(this).prev().append($($('#proyectoTemplate').html()));
            $('.add-select2').removeClass('.add-select2').select2();
            $('.add-datepicker').removeClass('.add-datepicker').datepicker();
            bind();
        });

        var COLOMBIA = false;
        var departamento_options = '<option value="">Departamento</option>';
        $.getJSON('/colombia.min.json', function(data) {
            COLOMBIA = data;
            // Genera los options de los departamentos:
            $.each(COLOMBIA, function(i, departamento) {
                departamento_options += '<option value="' + departamento.departamento + '">' + departamento.departamento + '</option>';
                pageSetup();
            });
        });

        var FORMACION_ACADEMICA = false;
        var formacion_options = '<option value=""></option>';
        $.getJSON('/formacion_academica.json', function(data) {
            FORMACION_ACADEMICA = data;
            // Genera los options de los departamentos:
            $.each(FORMACION_ACADEMICA, function(i, f) {
                formacion_options += '<option value="' + f + '">' + f + '</option>';
            });

            $('select[name=formacion_academica]').html(formacion_options);
        });

        (function() {
            var defaults = Plugin.getDefaults("wizard");
            var options = $.extend(true, {}, defaults, {
                templates: {
                    buttons: function() {
                        var options = this.options;
                        var html = '<div class="wizard-buttons">' +
                            '<a class="btn btn-default btn-outline" href="#' + this.id + '" data-wizard="back" role="button">Atrás</a>' +
                            '<a class="btn btn-success btn-outline float-right" href="#' + this.id + '" data-wizard="finish" role="button">Finalizar</a>' +
                            '<a class="btn btn-default btn-outline float-right" href="#' + this.id + '" data-wizard="next" role="button">Siguiente</a>' +
                            '</div>';
                        return html;
                    }
                },
                onInit: function() {
                    $('#PersonajesNew').formValidation({
                        framework: 'bootstrap4',
                        fields: {
                            documento: {
                                validators: {
                                    notEmpty: {
                                        message: 'El documento es requerido'
                                    }
                                }
                            }
                        },
                        err: {
                            clazz: 'text-help'
                        },
                        row: {
                            invalid: 'has-danger'
                        }
                    });
                },
                onNext: function() {
                    if ($('input[name=documento]').val() != '' && $("#wizardForm .step.active").data('target') == '#infoPersonal') {
                        // Busco la info del documento ingresado:
                        $('#wizardForm .panel-body').addClass('loading');
                        $.getJSON('/api/document/' + $('input[name=documento]').val(), function(data) {
                            $('#wizardForm .panel-body').removeClass('loading');
                            if (data && 'nombre' in data) {
                                $('input[name="nombre"], input[name="nombre_organizacion"]').val(data.nombre);
                                $('input[name="antecedentes_judiciales"]').val(data.antecedentes);
                                $('input[name="puesto_votacion_departamento"]').val(data.puesto_votacion.departamento);
                                $('input[name="puesto_votacion_municipio"]').val(data.puesto_votacion.municipio);
                                $('input[name="puesto_votacion_nombre"]').val(data.puesto_votacion.nombre);
                                $('input[name="puesto_votacion_direccion"]').val(data.puesto_votacion.direccion);
                                $('input[name="mesa_votacion"]').val(data.puesto_votacion.mesa);
                            } else {
                                if ('error' in data) {
                                    alertify.alert(data.error);
                                }

                                $("#wizardForm form")[0].reset();
                                $("#wizardForm").wizard('goTo', 0);
                            }
                        })
                    }
                },
                allowNext: false,
                validator: function() {
                    var fv = $('#PersonajesNew').data('formValidation');

                    var $this = $(this);

                    // Validate the container
                    fv.validateContainer($this);

                    var isValidStep = fv.isValidContainer($this);
                    if (isValidStep === false || isValidStep === null) {
                        return false;
                    }

                    return true;
                },
                onFinish: function() {
                    $('.contenedor-potencial, .build-container').each(function() {
                        $('.potencial, .build-index', this).each(function(a) {
                            $(this).find('input, select, textarea').each(function(e) {
                                if ($(this).attr('name')) {
                                    $(this).attr('name', $(this).attr('name').replace(/(\{n\})/ig, a));
                                }
                            });
                        });
                    });

                    if($('#upload-editor').is(':visible')) {
                        $uploadCrop.result({
                            type: 'canvas',
                            size: 'original'
                        }).then(function (resp) {
                            if(resp) {
                                $('#imagebase64').val(resp);
                            }

                            $('#PersonajesNew').off().submit();
                        });
                    } else {
                        $('#PersonajesNew').off().submit();
                    }
                },
                buttonsAppendTo: '.panel-body'
            });

            $("#wizardForm").wizard(options);
        })();

        function pageSetup() {
            $('.move-step').click(function() {
                $("#wizardForm").wizard('goTo', parseInt($(this).data('moveto')));
            });

            $('.potencial').each(function() {
                console.log($(this))
                bindMultiSelect($(this));
            });

            $('select[data-autoselect]').each(function() {
                var val = $(this).data('autoselect').split('|');
                for (var i in val) {
                    $(this).find('option[value="' + val[i] + '"]').prop('selected', true);
                }

                $(this).trigger('change');
            });

            $('div[data-autoselect]').each(function() {
                var val = $(this).data('autoselect').split('|');
                for (var i in val) {
                    $(this).find('input[value="' + val[i] + '"]').prop('checked', true);
                }
            });
            
        }

        function bindMultiSelect(a) {
            // Botón de eliminar
            $('.delete-this', a).click(function() {
                $(this).parents('.potencial').slideUp(function() {
                    $(this).remove();
                });
            });

            $('.departamento', a).each(function() {
                $(this).html(departamento_options).on('change', function() {
                    updateMunicipios($(this).parents('.row').find('.municipio'), $(this).val());
                });
            });
        }

        function updateMunicipios(el, n) {
            $.each(COLOMBIA, function(i, departamento) {
                if (departamento.departamento == n) {
                    if ('ciudades' in departamento) {
                        var ciudades = '';
                        for (var i in departamento.ciudades) {
                            ciudades += '<option value="' + departamento.ciudades[i] + '">' + departamento.ciudades[i] + '</option>';
                        }

                        el.html(ciudades).removeAttr('disabled');
                    } else {
                        el.attr('disabled', 'disabled');
                    }
                }
            })
        }

        function bind() {
            $('.eliminar-bloque').off().click(function() {
                if($(this).data('model') && $(this).data('id')) {
                    var el = $(this);
                    if(confirm('¿Seguro que desea eliminar este registro?')) {
                        $.post('/api/' + el.data('model')  + '/delete', {_id: el.data('id')}, function() {
                            el.parents('.potencial, .build-index').slideUp(function() {
                                $(this).next('hr').remove();
                                $(this).remove();
                            });
                        });
                    }
                } else {
                    $(this).parents('.potencial, .build-index').slideUp(function() {
                        $(this).next('hr').remove();
                        $(this).remove();
                    });
                }
            })
        }

         bind();
    });
});