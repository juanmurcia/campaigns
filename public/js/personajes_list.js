$(window).on('siteReady', function() {
    var organizacion_id = null;
    var organizacion_nombre = null;
    var personaje = null;

    $('.edit-organizacion').click(function() {
        organizacion_id = $(this).data('id');
        organizacion_nombre = $(this).data('nombre');
        personaje = $(this).data('personaje');

        if(organizacion_id && organizacion_nombre) {
            $('#organizacionUpdate select[name="organizacion_set"]').html(
                '<option value="' + organizacion_id + '" selected>' + organizacion_nombre + '</option>'
            ).trigger('change');
        }

        $('#organizacionUpdate').modal('show');
    });

    $('.update-organizacion').click(function() {
        if(personaje) {
            organizacion_id = $('#organizacionUpdate select[name="organizacion_set"]').val();
            organizacion_nombre = $('#organizacionUpdate select[name="organizacion_set"] option:selected').text();

            $('.edit-organizacion[data-personaje="' + personaje + '"]')
                .data('id', organizacion_id)
                .data('nombre', organizacion_nombre)
                .next().html('<a href="/admin/organizaciones/' + organizacion_id + '">' + organizacion_nombre + '</a>');

            $('#organizacionUpdate').modal('hide');

            // Ajax
            $.post('/api/personajes/' + personaje + '/organizacion', {organizacion: organizacion_id});
        }
    })
});