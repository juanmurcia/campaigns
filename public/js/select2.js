$(window).on('siteReady', function() {
  $('.selectAjax').each(function() {
    $(this).select2({
      placeholder: {
        id: '-1',
        text: 'Buscar...'
      },
      ajax: {
        url: "/api/search/" + $(this).data('model'),
        dataType: 'json',
        delay: 250,
        data: function (params) {
          return {
            q: params.term, // search term
            page: params.page
          };
        },
        cache: false
      },
      escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
      minimumInputLength: 3
    });
  });
  
  $('select[data-autoselect]').each(function() {
    var val = $(this).data('autoselect').split('|');
    for(var i in val) {
      $(this).find('option[value="' + val[i] + '"]').prop('selected', true);
    }
    
    $(this).trigger('change');
  });
});
