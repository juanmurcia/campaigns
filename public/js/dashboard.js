$(window).on('siteReady', function() {
  $('#ofertasForm').on('submit', function(e) {
    e.preventDefault();
    searchOfertas();
  });

  $('.filter-offers').click(searchOfertas);
});

function searchOfertas() {
  let tipo = $('#ofertasForm select[name="categoria"]').val().trim();
  let search = $('#ofertasForm input[name="buscar"]').val().trim();

  if(!tipo && !search) {
    alert('Debes seleccionar una categoría o ingresar un texto para buscar ofertas.');
    return;
  }

  $.getJSON('/api/search/ofertas?categoria=' + tipo + '&buscar=' + search,
    function(response) {
      if(response.length) {
        $('#ofertasModal .results-box').html($('#propuesta-tabla-template').html());

        for(var i in response) {
          for(var n in response[i]) {
            if(typeof response[i][n] == 'object') {
              for(var x in response[i][n]) {
                response[i][n + '_' + x] = response[i][n][x];
              }
            }
          }

          response[i].fecha_fin = moment(response[i].fecha_fin).format('YYYY-MM-DD');

          $('#ofertasModal .results-box tbody').append(
            replace($('#propuesta-template').html(), response[i])
          );
        }
      } else {
        $('#ofertasModal .results-box').html('<em>No se encontraron ofertas.</em>');
      }
    }
  );
}

function replace(str, vals) {
	for(var i in vals) {
		if(typeof vals[i] == 'string') {
      str = str.replace(new RegExp('\{' + i + '\}', 'ig'), vals[i]);
    }
	}
	return str;
}
