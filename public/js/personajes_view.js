$(document).ready(function() {
  $('.agregar-elemento').click(function() {
    $(this).toggleClass('hidden-xs-up');
    $(this).prev().toggleClass('hidden-xs-up');
  });

  $('.cancelar-elemento').click(function() {
    $(this).parents('form').toggleClass('hidden-xs-up');
    $(this).parents('form').next().toggleClass('hidden-xs-up');
  });

  $('.user-color .btn').click(function() {
    $(this).parent().find('.active').removeClass('active');
    $(this).addClass('active');
    $.post('/api/personajes/' + window._id + '/color', {set: $(this).data('color')});
  });

  $('#setContactos').click(function() {
    $.post('/api/personajes/' + window._id + '/agregado_contactos_google', {set: $(this).is(':checked')});
  });

  $('.actualizar-puesto').click(function() {
    $(this).parent().next().addClass('loader loader-default');
    $.get('/api/personajes/' + $(this).data('id') + '/puesto_votacion', function() {
      window.location.reload();
    });
    $(this).fadeOut();
  });

   $('#envio_mail, #llamada, #sms, #correspondencia').click(function() {
    $.post('/api/personajes/' + window._id + '/enviar.' + $(this).attr('name'), { set: $(this).is(':checked')});
  });

  $('.user-color').find('div[data-color="' + $('.user-color').data('select-color') + '"]').addClass('active');
});