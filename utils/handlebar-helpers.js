"use strict";

const moment = require('moment');
const _ = require('lodash');
const m2f = require('mimetype-to-fontawesome')({ prefix: 'fa-' })
const TIPOS_PROYECTO = {
    hoja_de_vida: "Hoja de Vida",
    comercial: "Comercial",
    primera_reunion: "Primera Reunión",
    visita_territorio: "Visita Territorio",
    gestion_entidad_privada: "Gestión entidad Privada",
    gestion_educativa: "Gestión educativa",
    gestion_entidad_publica: "Gestión entidad Pública",
    cita_interpersonal: "Cita Interpersonal",
    consecucion_recurso: "Consecusión recurso",
    programar_llamada: "Programar llamada",
    programar_reunion: "Programar reunión",
    realizar_pago: "Realizar pago",
    reunion_lideres: "Reunión líderes"
};

module.exports = (handlebars) => {
    let helpers = {
        lower: (str) => {
            return str.toLowerCase();
        },
        getModelPath: (str) => {
            return global.getModelPath(str);
        },
        raw: (options) => {
            return options.fn(this);
        },
        count: (data) => {
            return _.size(data);
        },
        sum: (data, key) => {
            let suma = 0;
            for (var i in data) {
                suma += (key in data[i] && !isNaN(parseInt(data[i][key]))) ? parseInt(data[i][key]) : 0;
            }
            return suma;
        },
        countCond: (data, key, operator, val) => {
            let count = 0;
            _.each(data, (el) => {
                switch (operator) {
                    case '==':
                        if (el[key] == val) { count++ }
                        break;
                    case '===':
                        if (el[key] === val) { count++ }
                        break;
                    case '!=':
                        if (el[key] != val) { count++ }
                        break;
                    case '!==':
                        if (el[key] !== val) { count++ }
                        break;
                    case '<':
                        if (el[key] < val) { count++ }
                        break;
                    case '<=':
                        if (el[key] <= val) { count++ }
                        break;
                    case '>':
                        if (el[key] > val) { count++ }
                        break;
                    case '>=':
                        if (el[key] >= val) { count++ }
                        break;
                    case '&&':
                        if (el[key] && val) { count++ }
                        break;
                    case '||':
                        if (el[key] || val) { count++ }
                        break;
                }
            });

            return count;
        },
        tipoProyecto: (tipo) => {
            return (tipo in TIPOS_PROYECTO) ? TIPOS_PROYECTO[tipo] : '';
        },
        estadoTarea: (tarea) => {
            return (moment(tarea.fin).isBefore(new Date())) ? 'expired' : 'active';
        },
        money: (amount) => {
            var formatMoney = (n, c, d, t) => {
                var c = isNaN(c = Math.abs(c)) ? 2 : c,
                    d = d == undefined ? "." : d,
                    t = t == undefined ? "," : t,
                    s = n < 0 ? "-" : "",
                    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
                    j = (j = i.length) > 3 ? j % 3 : 0;
                return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
            };

            return formatMoney(amount, 0);
        },
        countPercent: (data, key, operator, val) => {
            let count = 0;

            _.each(data, (el) => {
                switch (operator) {
                    case '==':
                        if (el[key] == val) { count++ }
                        break;
                    case '===':
                        if (el[key] === val) { count++ }
                        break;
                    case '!=':
                        if (el[key] != val) { count++ }
                        break;
                    case '!==':
                        if (el[key] !== val) { count++ }
                        break;
                    case '<':
                        if (el[key] < val) { count++ }
                        break;
                    case '<=':
                        if (el[key] <= val) { count++ }
                        break;
                    case '>':
                        if (el[key] > val) { count++ }
                        break;
                    case '>=':
                        if (el[key] >= val) { count++ }
                        break;
                    case '&&':
                        if (el[key] && val) { count++ }
                        break;
                    case '||':
                        if (el[key] || val) { count++ }
                        break;
                }
            });

            return (count * 100) / (_.size(data));
        },
        getIcon: (mime) => {
            return m2f(mime);
        },
        join: (data) => {
            if (_.isPlainObject(data[0]) && '_id' in data[0]) {
                let ids = [];
                _.each(data, (e) => { ids.push(e._id); });
                data = ids;
            }

            return data.join('|');
        },
        etiqueta: (e) => {
            e = e.split(':');
            return e[0].charAt(0).toUpperCase() + e[0].slice(1).toLowerCase() + ': ' + e[1];
        },
        date: (e) => { return (e) ? moment(e).format('YYYY-MM-DD') : ''; },
        dateTime: (e) => { return (e) ? moment(e).format('YYYY-MM-DD hh:mm A') : ''; },
        dateHour: (e) => { return (e) ? moment(e).format('hh:mm A') : ''; },
        dateInput: (e) => { return (e && moment(e).format('MM/DD/YYYY') != 'Invalid date') ? moment(e).format('MM/DD/YYYY') : ''; },
        toJSON: (obj) => {
            return JSON.stringify(obj);
        },
        get: (obj, key, match, get) => {
            for (var i in obj) {
                if (obj[i][key] == match) {
                    return obj[i][get];
                }
            }
            return '';
        },
        ifCond: (v1, operator, v2, options) => {
            switch (operator) {
                case '==':
                    return (v1 == v2) ? options.fn(this) : options.inverse(this);
                case '===':
                    return (v1 === v2) ? options.fn(this) : options.inverse(this);
                case '!=':
                    return (v1 != v2) ? options.fn(this) : options.inverse(this);
                case '!==':
                    return (v1 !== v2) ? options.fn(this) : options.inverse(this);
                case '<':
                    return (v1 < v2) ? options.fn(this) : options.inverse(this);
                case '<=':
                    return (v1 <= v2) ? options.fn(this) : options.inverse(this);
                case '>':
                    return (v1 > v2) ? options.fn(this) : options.inverse(this);
                case '>=':
                    return (v1 >= v2) ? options.fn(this) : options.inverse(this);
                case '&&':
                    return (v1 && v2) ? options.fn(this) : options.inverse(this);
                case '||':
                    return (v1 || v2) ? options.fn(this) : options.inverse(this);
                default:
                    return options.inverse(this);
            }
        }
    };

    for (var name in helpers) {
        handlebars.registerHelper(name, helpers[name]);
    }
};