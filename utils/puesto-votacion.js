"use strict";
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

const _           = require('lodash');
const request     = require('request');
const cheerio     = require('cheerio');
const diacritics  = require('diacritics').remove;
const iconv       = require('iconv');
const decode      = new iconv.Iconv('iso-8859-1', 'utf-8');

const ENDPOINT = "https://wsp.registraduria.gov.co/censo/_censoResultado.php?nCedula={documento}&nCedulaH=&x=84&y=11";

module.exports = (documento, callback) => {
  request({url: ENDPOINT.replace('{documento}', documento), method: 'GET', encoding: null}, (error, response, body) => {
    let infoPuesto = {
      departamento: '',
      municipio: '',
      direccion: '',
      mesa: '',
      nombre: ''
    };

    if(error) {
      console.log(error);
      callback(false);
      return false;
    }

    let $ = cheerio.load( decode.convert(body).toString('utf-8') );
    let table_td = $('#menuDer table td');

    _.each(table_td, (td, i) => {
      let value = $(table_td[i+1]).text();

      switch (diacritics($(td).text())) {
        case 'Departamento:':
          infoPuesto.departamento = value;
          break;
        case 'Municipio:':
          infoPuesto.municipio = value;
          break;
        case 'Puesto:':
          infoPuesto.nombre = value;
          break;
        case 'Direccion Puesto:':
          infoPuesto.direccion = value;
          break;
        case 'Mesa':
          infoPuesto.mesa = value;
          break;
      }
    });

    callback(infoPuesto);
  });
};
