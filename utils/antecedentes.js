"use strict";
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

const request     = require('request');
const cheerio     = require('cheerio');
const uuid        = require('uuid/v4');
const fs          = require('fs');

const FileCookieStore = require('tough-cookie-filestore');

const ENDPOINT = 'https://www.procuraduria.gov.co/CertWEB/Certificado.aspx?tpo=1';
const CAPTCHA = {
  "¿ Cual es la Capital de Colombia? (Sin tilde)": "bogota",
  "¿ Cuanto es 12+8?" : "20",
  "¿ Cuanto es 6 X 6?" : "36",
  "¿ Cuanto es 4+5?": "9",
  "¿ Cuanto es 2+20?": "22",
  "¿Número de colores en la bandera de Colombia?": "3",
  "¿Cuanto es 22+8?": "30",
  "¿ Cual es la Capital del Atlantico? (Sin tilde)": "Barranquilla",
  "¿ Cuanto es 5+3?": "8",
  "¿ Primer color de la bandera de Colombia?": "amarillo",
  "¿ Cuanto es 3 X 5?": "15"
}

module.exports = (documento, callback) => {
  let jar_id = uuid();
  fs.writeFileSync('./sessions/' + jar_id + '.json', '');

  let jar = request.jar(new FileCookieStore('./sessions/' + jar_id + '.json'));
  let tries = 0;
  let findCaptcha = (cb) => {
    if(tries === 5) {
      cb(false);
    }

    tries++;

    request.get({url: ENDPOINT, jar: jar}, (error, response, body) => {
      if(error) {
        findCaptcha(cb);
        return;
      }

      let $ = cheerio.load(body);

      if(typeof CAPTCHA[$('#lblPregunta').text()] != 'undefined') {
        let result = {};
        $('input, select', '#form1').each((i, el) => {
          if($(el).attr('name')) {
            result[$(el).attr('name')] = $(el).val();
          }
        });

        result.txtRespuestaPregunta = CAPTCHA[$('#lblPregunta').text()];
        result.txtNumID = documento;
        result.ddlTipoID = 1;

        cb(result);
      } else {
        // Busca otro captcha..
        findCaptcha(cb);
      }
    });
  }

  findCaptcha((data) => {
    if(!data) {
      callback(false);
      return;
    }

    data.__EVENTTARGET = '';
    data.__EVENTARGUMENT = '';

    request.post({
        url: ENDPOINT + '&' + Math.random(100, 1000),
        jar: jar,
        form: data,
        timeout: 60000
      },
    (error, response, body) => {
      fs.unlinkSync('./sessions/' + jar_id + '.json');
      if(error) {
        callback(false);
        return;
      }

      let $ = cheerio.load(body);

      if($('#ValidationSummary1').text().trim() != '') {
        console.log($('#ValidationSummary1').text().trim());
        callback(false);
        return;
      }

      let name = [];
      $('.datosConsultado span').each((i, el) => {
        name.push($(el).text());
      })

      name = name.join(' ');
      name = name.trim();

      callback({
        name, antecedentes: $('#divSec').html()
      });
    })
  });

  return true;
}
