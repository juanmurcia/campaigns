"use strict";
const moment = require('moment');
const async  = require('async');
const _      = require('lodash');

module.exports = () => {
  let min = new Date(moment().startOf('day').add(1, 'day').format('YYYY-MM-DD'));
  let max = new Date(moment().startOf('day').add(2, 'day').format('YYYY-MM-DD'));

  console.log('Obteniendo tareas de mañana...', min, max);

  async.parallel([
    (callback) => {
      global.models.Proyecto.find({finalizado: false,
            'tareas.finalizado': false,
            'tareas.fecha_inicio': {
              $gte: min,
              $lte: max
            }
          }).sort({'tareas.fecha_inicio': -1}).lean().exec((err, tareas) => {
          callback(err, tareas);
        });
    },
    (callback) => {
      global.models.User.find({}).lean().exec((err, users) => {
        callback(err, users);
      });
    }
  ], (err, data) => {
    if(err) {
      console.log('[reporte_dia] Error:', err);
    } else {
      let [proyectos, users] = data;
      let enviarTareas = {};

      console.log('proyectos', _.size(proyectos), 'usuarios', _.size(users));

      _.each(users, (usuario) => {
        enviarTareas[usuario._id] = [];
      });

      _.each(proyectos, (proyecto) => {
        for(var e in proyecto.tareas) {
          if(moment(proyecto.tareas[e].fecha_inicio).isBetween( min, max )) {
            enviarTareas[proyecto.tareas[e].responsable].push( proyecto.tareas[e] );
          }
        }
      });

      console.log(enviarTareas);
      for(var i in enviarTareas) {
        if(enviarTareas[i].length) {
          let user = users.find((usr) => { return usr._id.toString() == i; });

          global.sendMail(
            user.name,
            user.email,
            'Resumen de tus tareas para mañana',
            'resumen_tareas',
            {tareas: enviarTareas[i]},
            (status) => {}
          );
        }
      }

      console.log('Fin.');
    }
  });
};
