"use strict";
module.exports = (req, res) => {
  global.models.Oferta.remove({_id: req.params.id}).exec((err, model) => {
      if(err) {
        return res.redirect(req.header('Referer') || '/');
      } else {
        // Elimino del usuario
        global.models.Personaje.update({ofertas: req.params.id}, {$pull: {ofertas: req.params.id}}).exec((err, numAffected) => {
          global.models.Personaje.update({ofertas_usadas: req.params.id}, {$pull: {ofertas_usadas: req.params.id}}).exec((err, numAffected) => {
            return res.redirect('/admin/' + req.params.path);
          });
        });
      }
  });
}