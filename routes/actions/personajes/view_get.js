"use strict";

module.exports = (req, res) => {
    let model = global.models.Personaje.findOne({ _id: req.params.id });
    model = global.modelPopulate(model, 'personajes');
    model.lean().exec((err, found) => {
        if (err) {
            req.flash('error', err.message);
            return res.redirect('/admin');
        }

        if (found) {
            global.models.Proyecto.find({ personaje: found._id }).lean().exec((err, proyectos) => {
                if (proyectos) {
                    found.proyectos = proyectos;
                }
                
                res.render(req.params.path + '/view', found);
            });
        } else {
            res.redirect('/admin/' + req.params.path); // Not found
        }
    })
}