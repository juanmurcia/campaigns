"use strict";
const async = require('async');
const _ = require('lodash');

module.exports = (req, res, extra) => {
    async.parallel([
        (cb) => {
            global.models.Localidad.find({}, {nombre: 1}).lean().exec((err, localidades) => {
                cb(null, localidades);
            });
        },
        (cb) => {
            let model = global.models.Personaje.findOne({ _id: req.params.id });
            model = global.modelPopulate(model, 'personajes');
            model.lean().exec((err, personaje) => {
                if (personaje) {
                    // Obtiene los proyectos:
                    global.models.Proyecto.find({'personaje': personaje._id}).exec((err, proyectos) => {
                        if(proyectos) {
                            personaje.proyectos = proyectos;
                        }

                        cb(null, personaje);
                    });
                } else {
                    cb(true);
                }
            });
        },
        (cb) => {
            global.models.Organizacion.find({}, { nombre: 1 }).lean().exec((err, organizaciones) => {
                cb(null, organizaciones);
            });
        },
        (cb) => {
            global.models.User.find({}, { name: 1 }).lean().exec((err, usuarios) => {
                cb(null, usuarios);
            });
        }
    ], (err, data) => {
        if (err) {
            req.flash('error', err.message);
            res.redirect('/admin');
            return;
        }

        extra.localidades = data[0];
        extra.organizaciones = data[4];
        extra.usuarios = data[5];
        
        extra = _.merge(extra, data[3]);
        
        res.render(req.params.path + '/edit', extra);
    });

    return true;
}