"use strict";
const async = require('async');
const _ = require('lodash');

module.exports = (req, res, saved) => {
    async.waterfall([
        (cb) => {
            let puesto_votacion = {
                nombre: req.body.puesto_votacion_nombre,
                direccion: req.body.puesto_votacion_direccion,
                municipio: req.body.puesto_votacion_municipio,
                departamento: req.body.puesto_votacion_departamento
            };

            global.models.PuestoVotacion.findOne({ nombre: puesto_votacion.nombre }).lean().exec((err, puesto) => {
                if (!puesto) {
                    let new_puesto = new global.models.PuestoVotacion(puesto_votacion);
                    new_puesto.save((err, puesto) => {
                        saved.puesto_votacion = puesto._id;
                        saved.save((err) => {
                            cb(err);
                        });
                    })
                } else {
                    saved.puesto_votacion = puesto._id;
                    saved.save((err) => {
                        cb(err);
                    });
                }
            })
        },
        (cb) => {
            if (typeof cb != 'function') {
                req.flash('error', cb.message);
                cb = arguments[1]; // Select the next var, avoid error
            }

            // Actualizaciones del referido
            if ('referido' in saved && saved.referido) {
                global.models.Personaje.findOneAndUpdate({ _id: saved.referido }, { $addToSet: { referidos: saved._id } }, (err) => {
                    cb(err);
                });
            } else cb(null);
        },
        (cb) => {
             if (typeof cb != 'function') {
                req.flash('error', cb.message);
                cb = arguments[1]; // Select the next var, avoid error
            }

            // Acciones sobre la organización.
            if ('crear_organizacion' in req.body && req.body.crear_organizacion) {
                let organizacion = new global.models.Organizacion;
                organizacion.nombre = req.body.nombre_organizacion;
                organizacion.descripcion = req.body.descripcion_organizacion;
                organizacion.potencial_declarado = req.body.potencial_declarado_organizacion;
                organizacion.potencial_esperado = req.body.potencial_esperado_organizacion;
                organizacion.integrantes = [saved._id];
                if (req.body.representa_organizacion) {
                    organizacion.representantes = [saved._id];
                }

                organizacion.save((err, org) => {
                    saved.organizacion = org._id;
                    saved.save((err) => {
                        cb(err);
                    });
                });
            } else if (saved.organizacion) {
                let add_set = { integrantes: saved._id };
                if (req.body.representa_organizacion) {
                    add_set.representantes = saved._id;
                }

                global.models.Organizacion.findOneAndUpdate({ _id: saved.organizacion }, { $addToSet: add_set }, (err, organizacion) => {
                    cb(err);
                });
            } else {
                cb(null);
            }
        },
        (cb) => {
            if (typeof cb != 'function') {
                req.flash('error', cb.message);
                cb = arguments[1]; // Select the next var, avoid error
            }

            // Acciones sobre la entidad.
            if ('crear_entidad' in req.body && req.body.crear_entidad) {
                let entidad = new global.models.Entidad;
                entidad.nombre = req.body.nombre_entidad;
                entidad.direccion = req.body.direccion_entidad;
                entidad.telefono = req.body.telefono_entidad;

                entidad.save((err, entidad) => {
                    saved.entidad = entidad._id;
                    saved.save((err) => {
                        cb(err);
                    });
                });
            } else {
                cb(null);
            }
        },
        (cb) => {
            if (typeof cb != 'function') {
                req.flash('error', cb.message);
                cb = arguments[1]; // Select the next var, avoid error
            }

            if ('comentario' in req.body &&
                req.body.comentario &&
                req.body.comentario != '') {
                let comentario = new global.models.Comentario;
                comentario.usuario = global.user._id;
                comentario.referencia = saved._id;
                comentario.model = 'Personaje';
                comentario.comentario = req.body.comentario;

                comentario.save((err, comentario) => {
                    if (!err) {
                        saved.comentarios = [comentario._id];
                        saved.save((err) => {
                            cb(err);
                        })
                    } else {
                        cb(err);
                    }
                })
            } else {
                cb(null);
            }
        },
        (cb) => {
            if (typeof cb != 'function') {
                req.flash('error', cb.message);
                cb = arguments[1]; // Select the next var, avoid error
            }

            if ('proyecto' in req.body) {
                let asyncProyectos = [];
                _.each(req.body.proyecto, (proyecto) => {
                    asyncProyectos.push((callback) => {
                        proyecto.personaje = saved._id;
                        proyecto.creador = global.user._id;

                        global.models.Proyecto.create(proyecto, (error, saved) => {
                            callback(error, saved);
                        });
                    });
                });

                async.parallel(asyncProyectos, (err) => {
                    cb(err);
                });
            } else {
                cb(null);
            }
        },
        (cb) => {
            if (typeof cb != 'function') {
                req.flash('error', cb.message);
                cb = arguments[1]; // Select the next var, avoid error
            }

            if ('oferta' in req.body) {
                let asyncOfertas = [];
                _.each(req.body.oferta, (oferta) => {
                    asyncOfertas.push((callback) => {
                        oferta.personaje = saved._id;
                        oferta.usuario = global.user._id;

                        global.models.Oferta.create(oferta, (error, oferta) => {
                            global.models.Personaje.update({ _id: saved._id }, { $push: { ofertas: oferta._id } }).exec((err) => {
                                callback(error, oferta);
                            });
                        });
                    });
                });

                async.parallel(asyncOfertas, (err) => {
                    cb(err);
                });
            } else {
                cb(null);
            }
        }
    ], (err) => {
        if (err) {
            req.flash('error', err.message);
        }

        res.redirect(saved._id);
    });

    return true;
};