"use strict";
const async = require('async');
const _ = require('lodash');

module.exports = (req, res, saved) => {
    // Actualizo el usuario antes de hacer algo

    let old = saved.toObject();
    for (var n in req.body) {
        saved[n] = req.body[n];
    }

    if (req.body.eliminar_foto) {
        // Elimino la imagen que tiene:
        // TODO
        saved['foto_perfil'] = '';
    }

    async.waterfall([
        (cb) => {
            // Acciones sobre la organización.
            if ('crear_organizacion' in req.body && req.body.crear_organizacion) {
                let organizacion = new global.models.Organizacion;
                organizacion.nombre = req.body.nombre_organizacion;
                organizacion.descripcion = req.body.descripcion_organizacion;
                organizacion.potencial_declarado = req.body.potencial_declarado_organizacion;
                organizacion.potencial_esperado = req.body.potencial_esperado_organizacion;
                organizacion.integrantes = [saved._id];
                if (req.body.representa_organizacion) {
                    organizacion.representantes = [saved._id];
                }

                organizacion.save((err, org) => {
                    if (err) {
                        return cb(err);
                    }
                    saved.organizacion = org._id;
                    cb(null);
                });
            } else {
                cb(null);
            }
        }
    ], () => {
        saved.save((err) => {
            if (err) console.log(err);
            async.waterfall([
                (cb) => {
                    // Actualizaciones del referido
                    if ('referido' in saved && saved.referido && saved.referido != old.referido) {
                        // Actualizo al personaje anterior para sacarle el referido
                        global.models.Personaje.update({ _id: old.referido }, { $pull: { referidos: { _id: saved._id } } }, { safe: true }, (err) => {
                            // Actualizo al personaje nuevo para agregar el referido
                            if (err) {
                                return cb(err);
                            }

                            global.models.Personaje.findOneAndUpdate({ _id: saved.referido }, { $addToSet: { referidos: saved._id } }, (err) => {
                                cb(err);
                            });
                        });

                    } else cb(null);
                },
                (cb) => {
                    if (typeof cb != 'function') {
                        req.flash('error', cb.message);
                        return res.redirect('/admin/personajes/' + saved._id);
                    }

                    // Acciones sobre la organización.
                    if (saved.organizacion && saved.organizacion != old.organizacion) {
                        // Actualizo la organización anterior para sacar al usuario de ahi
                        global.models.Organizacion.update({ _id: old.organizacion }, { $pull: { integrantes: { _id: saved._id }, representantes: { _id: saved._id } } }, { safe: true }, (err) => {
                            // Actualizo la organización actual con el usuario
                            if (err) {
                                return cb(err);
                            }

                            let add_set = { integrantes: saved._id };

                            if (req.body.representa_organizacion) {
                                add_set.representantes = saved._id;
                            }

                            global.models.Organizacion.findOneAndUpdate({ _id: saved.organizacion }, { $addToSet: add_set }, (err) => {
                                cb(err);
                            });
                        });
                    } else {
                        cb(null);
                    }
                },
                (cb) => {
                    if (typeof cb != 'function') {
                        req.flash('error', cb.message);
                        return res.redirect('/admin/personajes/' + saved._id);
                    }

                    // Acciones sobre la entidad.
                    if ('crear_entidad' in req.body && req.body.crear_entidad) {
                        let entidad = new global.models.Entidad;
                        entidad.nombre = req.body.nombre_entidad;
                        entidad.direccion = req.body.direccion_entidad;
                        entidad.telefono = req.body.telefono_entidad;

                        entidad.save((err, entidad) => {
                            if (err) {
                                cb(err);
                            } else {
                                saved.entidad = entidad._id;
                                saved.save((err) => {
                                    cb(err);
                                });
                            }
                        });
                    } else {
                        cb(null);
                    }
                },
                (cb) => {
                    if (typeof cb != 'function') {
                        req.flash('error', cb.message);
                        return res.redirect('/admin/personajes/' + saved._id);
                    }
                    
                    if ('proyecto' in req.body) {
                        let asyncProyectos = [];
                        _.each(req.body.proyecto, (proyecto) => {
                            asyncProyectos.push((callback) => {
                                // Define si crea, o edita:
                                if('_id' in proyecto) {
                                    let proyecto_id = proyecto._id;
                                    delete proyecto._id;

                                    global.models.Proyecto.update({_id: proyecto_id}, proyecto, (error, saved) => {
                                        callback(error, saved);
                                    });
                                } else {
                                    proyecto.personaje = saved._id;
                                    proyecto.creador = global.user._id;

                                    global.models.Proyecto.create(proyecto, (error, saved) => {
                                        callback(error, saved);
                                    });
                                }
                            });
                        });

                        async.parallel(asyncProyectos, (err) => {
                            cb(err);
                        });
                    } else {
                        cb(null);
                    }
                },
                (cb) => {
                    if (typeof cb != 'function') {
                        req.flash('error', cb.message);
                        return res.redirect('/admin/personajes/' + saved._id);
                    }

                    if ('oferta' in req.body) {
                        let asyncOfertas = [];
                        _.each(req.body.oferta, (oferta) => {
                            asyncOfertas.push((callback) => {
                                // Define si crea, o edita:
                                if('_id' in oferta) {
                                    let oferta_id = oferta._id;
                                    delete oferta._id;

                                    global.models.Oferta.update({_id: oferta_id}, oferta, (error, oferta) => {
                                        callback(error, oferta);
                                    });
                                } else {
                                    oferta.personaje = saved._id;
                                    oferta.usuario = global.user._id;

                                    global.models.Oferta.create(oferta, (error, oferta) => {
                                        global.models.Personaje.update({ _id: saved._id }, { $push: { ofertas: oferta._id } }).exec((err) => {
                                            callback(error, oferta);
                                        });
                                    });
                                }
                            });
                        });

                        async.parallel(asyncOfertas, (err) => {
                            cb(err);
                        });
                    } else {
                        cb(null);
                    }
                }
            ], function(err) {
                if (err) {
                    req.flash('error', err.message);
                }
                res.redirect('/admin/personajes/' + saved._id);
            });
        });
    });

    return true;
};