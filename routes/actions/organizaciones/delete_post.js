"use strict";
module.exports = (req, res) => {
  global.models.Organizacion.remove({_id: req.params.id}).exec((err, model) => {
      if(err) {
        return res.redirect(req.header('Referer') || '/');
      } else {
        global.models.Personaje.update({organizacion: req.params.id}, {$set: {organizacion: null}}).exec((err, numAffected) => {
          return res.redirect('/admin/' + req.params.path);
        });
      }
  });
}