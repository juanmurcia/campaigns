"use strict";
const async = require('async');
const _ = require('lodash');

module.exports = (req, res) => {
    // Hago busqueda en diferentes modelos, unifico el restultado
    // y la vista lo muestra..

    async.parallel([
        (cb) => {
            global.models.Personaje.find({
                $text: {$search: req.query.text}
            }, {
                nombre: 1,
                score: {$meta: "textScore"}
            }).sort({score:{$meta:"textScore"}}).exec((err, personajes) => {
                cb(err, personajes);
            });
        },
        (cb) => {
            global.models.Organizacion.find({
                $text: {$search: req.query.text}
            }, {
                nombre: 1,
                score: {$meta: "textScore"}
            }).sort({score:{$meta:"textScore"}}).exec((err, organizaciones) => {
                cb(err, organizaciones);
            });
        },
        (cb) => {
            global.models.Comentario.find({
                $text: {$search: req.query.text}
            }, {
                comentario: 1,
                referencia: 1,
                model: 1,
                score: {$meta: "textScore"}
            }).sort({score:{$meta:"textScore"}}).exec((err, comentarios) => {
                cb(err, comentarios);
            });
        },
        (cb) => {
            global.models.Oferta.find({
                $text: {$search: req.query.text}
            }, {
                tipo: 1,
                descripcion: 1,
                score: {$meta: "textScore"}
            }).sort({score:{$meta:"textScore"}}).exec((err, ofertas) => {
                cb(err, ofertas);
            });
        },
        (cb) => {
            global.models.Proyecto.find({
                $text: {$search: req.query.text}
            }, {
                titulo: 1,
                score: {$meta: "textScore"}
            }).sort({score:{$meta:"textScore"}}).exec((err, proyectos) => {
                cb(err, proyectos);
            });
        },
    ], (err, data) => {
        if (err) {
            req.flash('error', err.message);
            return res.redirect('/admin');
        }

        let [personajes, organizaciones, comentarios, ofertas, proyectos] = data;
        res.render('search', { personajes, organizaciones, comentarios, ofertas, proyectos });
    });

    return true;
}