"use strict";
const async = require('async');
const _ = require('lodash');

module.exports = (req, res, extra) => {
    async.parallel([
        (cb) => {
            global.models.User.find({}, { name: 1 }).lean().exec((err, usuarios) => {
                cb(null, usuarios);
            });
        },
        (cb) => {
            let model = global.models.Proyecto.findOne({ _id: req.params.id });
            model = global.modelPopulate(model, req.params.path);
            model.lean().exec((err, proyecto) => {
                cb(null, proyecto);
            });
        },
        (cb) => {
            global.models.Organizacion.find({}, { nombre: 1 }).lean().exec((err, organizaciones) => {
                cb(null, organizaciones);
            });
        }
    ], (err, data) => {
        if (err) {
            req.flash('error', err.message);
            res.redirect('/admin');
            return;
        }

        let [users, proyecto, organizaciones] = data;
        proyecto.usuarios = users;
        proyecto.organizaciones = organizaciones;

        res.render(req.params.path + '/edit', proyecto);
    });

    return true;
}