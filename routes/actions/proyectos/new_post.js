"use strict";
const _ = require('lodash');
const async = require('async');

module.exports = (req, res, proyecto) => {
    global.models.User.find({ _id: proyecto.usuario_responsable }).lean().exec((err, users) => {
        if (!err) {
            let asyncMail = [];
            _.each(users, (usuario) => {
                asyncMail.push((callback) => {
                    global.sendMail(
                        usuario.name,
                        usuario.email,
                        'Se ha creado un proyecto',
                        'new_proyect.handlebars', { _id: proyecto._id, nombre: proyecto.titulo },
                        () => {
                            callback(null);
                        }
                    );
                });
            });

            async.parallel(asyncMail, (err) => {
                if (err) {
                    req.flash('error', err.message);
                }
                res.redirect(proyecto._id);
            });
        } else {
            res.redirect(proyecto._id);
        }
    });
};