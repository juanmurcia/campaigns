"use strict";
const async = require('async');
const _ = require('lodash');
const columnas = {
    reunion: 'Reunión',
    gestion_entidad_publica: 'Gestión entidad Pública',
    gestion_entidad_privada: 'Gestión entidad Privada',
    gestion_educativa: 'Gestión educativa',
    hoja_de_vida: 'Tramitar Hoja de Vida',
    realizar_pago: 'Realizar pago',
    consecucion_recurso: 'Consecución Recursos Economicos',
    programar_llamada: 'Programar llamada',
    programar_reunion: 'Programar reunión',
    comercial: 'Comercial',
    compromiso_congresista: 'Compromiso congresista',
    presentacion_lideres: 'Presentación de Líder(es)'
};

module.exports = (req, res) => {
    async.parallel([
        (cb) => {
            let query = { finalizado: false, 'tareas.responsable': global.user._id };

            global.models.Proyecto.find(query).populate('personaje tareas.personaje').populate({
                path: 'tareas.comentarios',
                populate: { path: 'usuario' }
            }).sort({'tareas.fecha_fin': 1}).lean().exec((err, found_projects) => {
                if (err) {
                    cb(err);
                }

                let tareas = [];

                _.each(found_projects, (project) => {
                    if ('tareas' in project && project.tareas) {
                        _.each(project.tareas, (tarea) => {
                            if (tarea.responsable == global.user._id) {
                                tareas.push({
                                    _id: tarea._id,
                                    proyecto: project,
                                    categoria: columnas[project.tipo],
                                    titulo: tarea.titulo,
                                    descripcion: tarea.descripcion,
                                    inicio: tarea.fecha_inicio,
                                    fin: tarea.fecha_fin,
                                    comentarios: ('comentarios' in tarea) ? tarea.comentarios : [],
                                    estado: ('estado' in tarea) ? tarea.estado : 'PENDIENTE',
                                    personaje: tarea.personaje
                                });
                            }
                        });
                    }
                });

                cb(null, tareas);
            });
        }
    ], (err, data) => {
        if (err) {
            req.flash('error', err.message);
            return res.redirect('/admin');
        }

        let [tareas] = data;

        let columnas_vista = [];
        for(var i in columnas) {
            let columna = {nombre: columnas[i], tareas: []};

            _.each(tareas, (tarea) => {
                if(tarea.categoria == columnas[i]) {
                    columna.tareas.push(tarea);
                }
            });

            columnas_vista.push(columna);
        }

        res.render('tareas/list', { usuario: global.user, columnas: columnas_vista});
    });

    return true;
}