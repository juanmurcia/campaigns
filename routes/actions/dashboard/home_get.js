"use strict";
const _ = require('lodash');
const async = require('async');
const moment = require('moment');

module.exports = (req, res) => {
    let total = 0;
    async.parallel([
        (cb) => {
            global.models.Log.find({}).populate('user document').sort({ createdAt: 'desc' }).limit(10).lean().exec((err, logs) => {
                cb(err, logs);
            });
        },
        (cb) => {
            global.models.Organizacion.aggregate({
                $group: {
                    _id: '',
                    potencial_declarado: { $sum: '$potencial_declarado' },
                    potencial_esperado: { $sum: '$potencial_esperado' }
                }
            }, {
                $project: {
                    _id: 0,
                    potencial_declarado: '$potencial_declarado',
                    potencial_esperado: '$potencial_esperado'
                }
            }, (err, data) => {
                cb(err, data);
            });
        },
        (cb) => {
            global.models.Proyecto.find({
                finalizado: false,
                'tareas.responsable': global.user._id,
                'tareas.finalizado': false,
                'tareas.fecha_inicio': {
                    $gte: moment().startOf('day').toDate(),
                    $lte: moment().endOf('day').toDate()
                }
            }).lean().exec((err, proyects) => {
                if (err) {
                    return cb(err);
                }

                let todo = [];
                _.each(proyects, (proyect) => {
                    if ('tareas' in proyect && proyect.tareas) {
                        _.each(proyect.tareas, (tarea) => {
                            var time = moment(tarea.fecha_inicio).unix();
                            var start = moment().startOf('day').unix();
                            var end = moment().endOf('day').unix();

                            tarea.proyecto_id = proyect._id;
                            if (tarea.responsable == global.user._id &&
                                time >= start && time <= end) {
                                todo.push(tarea);
                            }
                        })
                    }
                });

                cb(null, todo);
            });
        },
        (callback) => {
            let nextDays = [];
            let today = Date.now();
            let oneday = (1000 * 60 * 60 * 24);
            let inDays = Date.now() + (oneday * 7);

            //  make an array of all the month/day combos for the next 30 days    
            for (var i = today; i < inDays; i = i + oneday) {
                let thisday = new Date(i);
                nextDays.push({
                    "m": thisday.getMonth() + 1,
                    "d": thisday.getDate()
                });
            }

            global.models.Personaje.aggregate([{
                    $project: {
                        m: { $cond: [{ $ifNull: ['$fecha_nacimiento', false] }, { $month: '$fecha_nacimiento' }, -1] },
                        d: { $cond: [{ $ifNull: ['$fecha_nacimiento', false] }, { $dayOfMonth: '$fecha_nacimiento' }, -1] },
                        nombre: "$nombre"
                    }
                },
                {
                    $match: {
                        $or: nextDays,
                        deleted: {
                            $ne: true
                        }
                    }
                },
                {
                    $group: {
                        _id: {
                            month: "$m",
                            day: "$d",
                        },
                        personajes: { $push: { _id: "$_id", nombre: "$nombre" } }
                    }
                }
            ]).exec((err, personajes_cumple) => {
                callback(err, personajes_cumple);
            });
        }
    ], (err, data) => {
        if (err) {
            req.flash('error', err.message);
            return res.render('home');
        }

        return res.render('home', { logs: data[0], totales: data[1][0], tareas: data[2], personajes_cumple: data[3] });
    })
}