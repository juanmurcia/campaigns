"use strict";
process.env.TZ = 'Americas/Bogota';

const express = require('express');
const route = express.Router();
const jwt = require('jsonwebtoken');
const uuid = require('uuid/v4');
const fs = require('fs');
const multer = require('multer');
const async = require('async');

const moment = require('moment');
const _ = require('lodash');
const auth = require('../middleware/auth');
const bcrypt = require('bcrypt-nodejs');
const csrf = require('csurf');
const csrfProtection = csrf({ cookie: true });


const puestoVotacion = require('../utils/puesto-votacion');

// File upload settings:

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './public/uploads');
    },
    filename: function(req, file, cb) {
        cb(null, uuid() + '.' + file.mimetype.split('/')[1])
    }
});

const fileFilter = (req, file, cb) => {
    // To reject this file pass `false`, like so:
    //cb(null, false)
    // To accept the file pass `true`, like so:
    //cb(null, true)
    // You can always pass an error if something goes wrong:
    //cb(new Error('I don\'t have a clue!'))
    cb(null, true);
}

// `upload` middleware for file uploads.
const upload = multer({ storage: storage, fileFilter: fileFilter });

module.exports = (app, mongoose) => {
    route.get('/', auth, (req, res) => {
        require('./actions/dashboard/home_get')(req, res);
    });

    route.get('/login', csrfProtection, (req, res) => {
        // Login para el admin
        res.render('login', { csrfToken: req.csrfToken(), layout: 'layouts/login' });
    });

    route.get('/error-test', (req, res) => {
        req.flash('error', 'Error message test');
        res.redirect('/admin/');
    });

    route.get('/logout', auth, (req, res) => {
        // Logout para el admin
        res.cookie('jwt_auth', '', { expires: new Date(), httpOnly: true });
        res.redirect('/admin/');
    });

    app.get('/forgot', csrfProtection, (req, res) => {
        res.render('forgot', { csrfToken: req.csrfToken(), layout: 'layouts/login' });
    });

    app.post('/forgot', csrfProtection, (req, res) => {
        global.models.User.findOne({ email: req.body.email }).exec((err, user) => {
            if (err) return res.redirect('/admin');
            user.recover_string = uuid();
            user.save((err, usuario) => {
                if (err) {
                    req.flash('error', err.message);
                    return res.redirect('/admin/');
                }

                global.sendMail(
                    usuario.name,
                    usuario.email,
                    'Define una nueva contraseña para tu cuenta',
                    'forgot.handlebars', { token: usuario.recover_string },
                    () => {
                        res.render('forgot_sent', { layout: 'layouts/login' });
                    }
                );
            });
        });
    });

    app.get('/recover', csrfProtection, (req, res) => {
        // Busco el usuario que coincida con el link de recover:
        global.models.User.findOne({ recover_string: req.query.token }).exec((err, user) => {
            if (!err && user) {
                // Login para el admin
                res.render('recover', { csrfToken: req.csrfToken(), layout: 'layouts/login' });
            } else {
                req.flash('error', err.message);
                res.redirect('/admin');
            }
        });
    });

    app.post('/recover', csrfProtection, (req, res) => {
        if (req.body.password != req.body.confirm) {
            return res.redirect('/admin');
        }

        // Busco el usuario que coincida con el link de recover:
        global.models.User.findOne({ recover_string: req.query.token }).exec((err, user) => {
            if (!err && user) {
                // Actualizo el pass
                bcrypt.hash(req.body.password, null, null, function(err, hashed) {
                    if (err) throw err;

                    user.password = hashed;
                    user.recover_string = null;
                    user.save(() => {
                        res.redirect('/admin');
                    });
                });
            } else {
                req.flash('error', err.message);
                res.redirect('/admin');
            }
        });
    });

    route.get('/search', auth, (req, res) => {
        require('./actions/search')(req, res);
    });

    route.post('/upload', auth, (req, res) => {
        res.redirect('/admin/');
    });

    route.get('/migrate', (req, res) => {
        let cb_length = 0;
        global.models.Personaje.find({}).exec((err, personajes) => {
            _.each(personajes, (personaje) => {
                if (!('formacion_academica' in personaje) || personaje.formacion_academica == '') {
                    cb_length++;
                    return;
                }

                personaje.formacion = [{
                    nivel_academico: personaje.nivel_academico,
                    descripcion: personaje.formacion_academica
                }];

                delete personaje.nivel_academico;
                delete personaje.formacion_academica;

                personaje.save((err) => {
                    if (err) {
                        req.flash('error', err.message);
                    }

                    cb_length++;
                    if (cb_length == personajes.length) {
                        return res.redirect('/admin');
                    }
                });
            });
        });
    });

    route.post('/personajes/:id/offer', auth, (req, res) => {
        // Dejar comentario a un personaje
        global.models.Personaje.findOne({ _id: req.params.id }).exec((err, personaje) => {
            if (err || !personaje) {
                return res.redirect(req.header('Referer') || '/');
            }

            let oferta = global.models.Oferta({
                categoria: req.body.categoria,
                usuario: global.user._id,
                personaje: personaje._id,
                tipo: req.body.tipo,
                descripcion: req.body.descripcion,
                fecha_fin: req.body.fecha_fin
            });

            oferta.save((err, saved) => {
                if (!err) {
                    if (!('ofertas' in personaje)) {
                        personaje.ofertas = [];
                    }

                    personaje.ofertas.push(saved._id);
                    personaje.save(() => {
                        return res.redirect(req.header('Referer') || '/');
                    });
                } else {
                    req.flash('error', err.message);
                    return res.redirect(req.header('Referer') || '/');
                }
            })
        })
    });

    route.get('/testuser', (req, res) => {
        bcrypt.hash('bogotasoyyo', null, null, function(err, hashed) {
            if (err) throw err;

            global.models.User.create([{
                name: 'Manuel Medina',
                email: 'omarmanuelmedina@gmail.com',
                password: hashed
            }, {
                name: 'Jorge Alvira',
                email: 'jorgeandresalviracruz@gmail.com',
                password: hashed
            }], () => {
                res.redirect('/admin/');
            });
        });
    });

    route.post('/login', csrfProtection, (req, res) => {
        // find the user
        global.models.User.findOne({
            email: req.body.email,
            active: true
        }, function(err, client) {
            if (err) {
                req.flash('error', err.message);
                return res.redirect('/admin/login');
            }

            if (!client) {
                res.redirect('/admin/login');
            } else {
                bcrypt.compare(req.body.password, client.password, function(err, validation) {
                    if (err) {
                        return;
                    }

                    // check if password matches
                    if (!validation) {
                        res.redirect('/admin/login');
                    } else {
                        // create a token
                        var token = jwt.sign(client, global.secret, {
                            expiresIn: (60 * 60 * 24) * 7 // expires in 24 hours
                        });

                        res.cookie('jwt_auth', token, { expires: new Date(2147483647000), httpOnly: true, sameSite: true });
                        res.redirect('/admin/');
                    }
                });
            }

        });
    });

    route.get('/logout', auth, (req, res) => {
        req.session.destroy(function(err) {
            res.clearCookie('jwt_auth', { httpOnly: true, sameSite: true });
            res.redirect('/'); //Inside a callback… bulletproof!
        });
    })

    // VIEW Proyectos
    route.get('/proyectos', auth, (req, res) => {
        require('./actions/proyectos/list_get')(req, res);
    });

    // VIEW Proyectos
    route.get('/proyectos/new', auth, (req, res) => {
        require('./actions/proyectos/new_get')(req, res);
    });

    route.post('/proyectos/:id/eliminar_tarea', auth, (req, res) => {
        global.models.Proyecto.update({ _id: req.params.id }, { $pull: { tareas: { _id: req.body.id } } }).exec((err, updated) => {
            if (err) {
                req.flash('error', err.message);
            }
            return res.redirect(req.header('Referer') || '/');
        })
    });

    route.post('/proyectos/:id/tarea', auth, (req, res) => {
        let tarea = {
            titulo: req.body.titulo,
            descripcion: req.body.descripcion,
            responsable: req.body.responsable,
            personaje: req.body.personaje,
            fecha_inicio: moment(req.body.fecha_inicio + ' ' + req.body.fecha_inicio_time, 'MM/DD/YYYY hh:mma').toDate(),
            fecha_fin: moment(req.body.fecha_inicio + ' ' + req.body.fecha_fin_time, 'MM/DD/YYYY hh:ma').toDate(),
            fecha_creacion: new Date,
            creado: global.user._id
        };

        // Dejar comentario a un modelo
        global.models.Proyecto.findOne({ _id: req.params.id }).exec((err, proyecto) => {
            if (err || !proyecto) {
                req.flash('error', err.message);
                return res.redirect(req.header('Referer') || '/');
            }

            if (!('tareas' in proyecto)) {
                proyecto.tareas = [];
            }

            proyecto.tareas.push(tarea);
            proyecto.save((err, saved) => {
                if (err) {
                    req.flash('error', err.message);
                }

                let usuarios = _.union(proyecto.usuario_responsable, [req.body.responsable]);

                global.models.User.find({ _id: usuarios }).lean().exec((err, users) => {
                    if (!err) {
                        let asyncMail = [];
                        _.each(users, (usuario) => {
                            asyncMail.push((callback) => {
                                if (usuario._id.toString() != req.body.responsable) {
                                    global.sendMail(
                                        usuario.name,
                                        usuario.email,
                                        'Se ha creado una nueva tarea al proyecto ' + proyecto.titulo,
                                        'new_task_notice.handlebars', { _id: proyecto._id, nombre: tarea.titulo },
                                        () => {
                                            callback(null);
                                        }
                                    );
                                } else {
                                    global.sendMail(
                                        usuario.name,
                                        usuario.email,
                                        'Tienes una nueva tarea asignada',
                                        'new_task.handlebars', { _id: proyecto.tareas[proyecto.tareas.length - 1]._id, proyecto_id: proyecto._id },
                                        () => {
                                            callback(null);
                                        }
                                    );
                                }
                            });
                        });

                        async.parallel(asyncMail, (err) => {
                            return res.redirect(req.header('Referer') || '/');
                        });
                    } else {
                        req.flash('error', err.message);
                        res.redirect(proyecto._id);
                    }
                });
            })
        })
    });


    // CRUD Universal:

    // VIEW ALL
    route.get('/:path', auth, (req, res) => {
        switch (req.params.path) {
            case 'tareas':
                require('./actions/tareas/list_get')(req, res);
                return;
                break;
        }

        let model = global.getModel(req.params.path);
        if (!model) return res.redirect('/admin');

        let query = {};

        if ('search' in req.query) {
            switch (req.params.path) {
                case 'organizaciones':
                    query = { nombre: new RegExp(req.query.search, 'ig') };
                    break;

                case 'personajes':
                    switch (req.query.field) {
                        case 'nombre':
                            query = { nombre: new RegExp(req.query.search, 'ig') };
                            break;
                        case 'email':
                            query = { email_personal: new RegExp(req.query.search, 'ig') };
                            break;
                        case 'telefono':
                            query = {
                                $or: [
                                    {telefono_movil: new RegExp(req.query.search, 'ig')},
                                    {telefono_movil_alterno: new RegExp(req.query.search, 'ig')},
                                    {'telefono_fijo.numero': new RegExp(req.query.search, 'ig')}
                                ]
                            };
                            break;
                        case 'documento':
                            query = { documento: parseInt(req.query.search) };
                            break;
                        case 'formacion':
                            query = {
                                $or: [
                                    {'formacion.nivel_academico': new RegExp(req.query.search, 'ig')},
                                    {'formacion.descripcion': new RegExp(req.query.search, 'ig')}
                                ]
                            };
                            break;
                    }

                    if (req.query.referido) {
                        query.referido = req.query.referido;
                    }

                    if (req.query.organizacion) {
                        query.organizacion = req.query.organizacion;
                    }

                    if (req.query.localidad) {
                        query.localidad = req.query.localidad;
                    }
                    break;
            }
        }

        model = (req.query.deleted) ? model.findDeleted(query) : model.find(query);
        model = global.modelPopulate(model, req.params.path);

        let skipped = 0;
        let next_page = 2;

        if ('page' in req.query) {
            if (!isNaN(parseInt(req.query.page))) {
                let page = parseInt(req.query.page)
                skipped = page * 50;
                next_page = page + 1;
                model = model.skip(skipped);
            }
        }

        switch (req.params.path) {
            case 'personajes':
                model = model.sort({'nombre': 1});
            break;
        }

        if('export' in req.query) {
            if(req.query.export == 'csv') {
                model.lean().exec((err, found) => {
                    let csv = [];
                    switch (req.params.path) {
                        case 'personajes':
                            csv.push({
                                nombre: 'Nombre',
                                documento: 'Documento',
                                email: 'Email Personal',
                                sexo: 'Sexo',
                                //descripcion: 'Descripción',
                                telefono_movil: 'Teléfono móvil',
                                telefono_movil_alterno: 'Teléfono móvil alterno',
                                direccion_residencia: 'Dirección residencia',
                                organizacion: 'Organización',
                                referido: 'Referido por'
                            });
                        break;
                    }
                    _.each(found, (e) => {
                        switch (req.params.path) {
                            case 'personajes':
                                csv.push({
                                    nombre: e.nombre,
                                    documento: e.documento,
                                    email: e.email_personal,
                                    sexo: e.sexo,
                                    //descripcion: e.descripcion,
                                    telefono_movil: e.telefono_movil,
                                    telefono_movil_alterno: e.telefono_movil_alterno,
                                    direccion_residencia: e.direccion_residencia,
                                    organizacion: (e.organizacion) ? e.organizacion.nombre : '',
                                    referido: (e.referido) ? e.referido.nombre : ''
                                });
                            break;
                        }
                    });

                    return res.csv(csv);
                });
                
                return;
            }
        }

        model.limit(50).lean().exec((err, found) => {
            if (err) {
                req.flash('error', err.message);
                return res.redirect('/admin');
            }

            let size = _.size(found);

            let counter = global.getModel(req.params.path);
            counter = (req.query.deleted) ? counter.countDeleted(query) : counter.count(query);
            counter.exec((err, total) => {
                let response = {
                    results: found,
                    query: req.query,
                    total: total,
                    max: size + skipped,
                    next_page: next_page,
                    deleted_view: (req.query.deleted)
                };

                switch (req.params.path) {
                    case 'personajes':
                        if(req.query.deleted) {
                            response.deleted = 0;
                        } else {
                            global.models.Personaje.countDeleted({}).exec((err, deleted) => {
                                response.deleted = deleted;
                                res.render(req.params.path + '/list', response);
                            });
                            return;
                        }
                    break;
                }
                
                res.render(req.params.path + '/list', response);
            });
        });
    });

    // NEW
    route.get('/:path/new', auth, (req, res) => {
        let extra = {}; // Se llena con modelos extra para construir los new

        switch (req.params.path) {
            case 'personajes':
                return require('./actions/personajes/new_get')(req, res, extra);
        }

        res.render(req.params.path + '/new', extra);
    });

    // DELETE
    route.get('/:path/:id/delete', auth, (req, res) => {
        res.render(req.params.path + '/delete');
    });

    route.post('/:path/:id/delete', auth, (req, res) => {
        if (!req.body.eliminar) {
            return res.redirect(req.header('Referer') || '/');
        }

        switch (req.params.path) {
            case 'organizaciones':
                return require('./actions/organizaciones/delete_post')(req, res);
            case 'ofertas':
                return require('./actions/ofertas/delete_post')(req, res);
        }

        let model = global.getModel(req.params.path);
        if (!model) return res.redirect('/admin');

        model.findOne({ _id: req.params.id }).exec((error, record) => {
            if (!error) {
                record.delete(mongoose.Types.ObjectId(global.user._id), (err) => {
                    if (err) {
                        req.flash('error', err.message);
                        return res.redirect(req.header('Referer') || '/');
                    } else {
                        return res.redirect('/admin/' + req.params.path);
                    }
                })
            } else {
                req.flash('error', err.message);
                return res.redirect(req.header('Referer') || '/');
            }
        });
    });

    // RESTORE
    route.get('/:path/:id/restore', auth, (req, res) => {
        res.render(req.params.path + '/restore');
    });

    route.post('/:path/:id/restore', auth, (req, res) => {
        if (!req.body.restore) {
            req.flash('error', 'Debes marcar que estas seguro para continuar.');
            return res.redirect(req.header('Referer') || '/');
        }

        let model = global.getModel(req.params.path);
        if (!model) return res.redirect('/admin');

        model.findOne({ _id: req.params.id }).exec((error, record) => {
            if (!error) {
                record.restore((err) => {
                    if (err) {
                        req.flash('error', err.message);
                        return res.redirect(req.header('Referer') || '/');
                    } else {
                        return res.redirect('/admin/' + req.params.path);
                    }
                })
            } else {
                req.flash('error', err.message);
                return res.redirect(req.header('Referer') || '/');
            }
        });
    });

    route.get('/:path/:id/edit', auth, (req, res) => {
        let extra = {}; // Se llena con modelos extra para construir los new

        switch (req.params.path) {
            case 'personajes':
                return require('./actions/personajes/edit_get')(req, res, extra);
            case 'proyectos':
                return require('./actions/proyectos/edit_get')(req, res);
        }

        let model = global.getModel(req.params.path);
        if (!model) return res.redirect(req.header('Referer') || '/');

        model = model.findOne({ _id: req.params.id });
        model = global.modelPopulate(model, req.params.path);
        model.exec((err, found) => {
            if (err) {
                req.flash('error', err.message);
                return res.redirect(req.header('Referer') || '/');
            }

            if (found) {
                res.render(req.params.path + '/edit', found);
            } else {
                res.redirect('/admin/' + req.params.path); // Not found
            }
        });
    });

    // INSERT
    route.post('/:path/new', [auth], (req, res) => {
        let model = global.getModel(req.params.path);
        if (!model) return res.redirect(req.header('Referer') || '/');

        req.body.creador = global.user._id;
        switch (req.params.path) {
            case 'personajes':
                //if (req.body.direccion_residencia) req.body.direccion_residencia = req.body.direccion_residencia.join(' ');
                req.body.redes_sociales = [];

                if (req.body.twitter) req.body.redes_sociales.push({ red: 'Twitter', url: req.body.twitter });
                if (req.body.facebook) req.body.redes_sociales.push({ red: 'Facebook', url: req.body.facebook });
                if (req.body.instagram) req.body.redes_sociales.push({ red: 'Instagram', url: req.body.instagram });
                if ('pic_base64' in req.body && req.body.pic_base64.length) {
                    let pic = req.body.pic_base64.replace(/^data:image\/png;base64,/, "");
                    let name = uuid() + req.body.ext;
                    let filename = './public/uploads/' + name;
                    
                    fs.writeFileSync(filename, pic, 'base64');

                    req.body.foto_perfil = name;
                }

                if (req.body.fecha_nacimiento && moment(req.body.fecha_nacimiento, 'MM/DD/YYYY').toDate() != 'Invalid Date') {
                    req.body.fecha_nacimiento = moment(req.body.fecha_nacimiento, 'MM/DD/YYYY').toDate();
                }
                break;
            case 'proyectos':
                break;
        }

        let newRecord = new model(req.body);

        newRecord.save((err, saved) => {
            if (err) {
                console.log(err);
                return res.redirect(req.header('Referer') || '/');
            }

            if (saved) {
                switch (req.params.path) {
                    case 'personajes':
                        return require('./actions/personajes/new_post')(req, res, saved);
                    case 'proyectos':
                        return require('./actions/proyectos/new_post')(req, res, saved);
                    default:
                        res.redirect(saved._id);
                        break;
                }
            }
        });
    });

    // UPDATE
    route.post('/:path/:id/edit', [auth], (req, res) => {
        let model = global.getModel(req.params.path);
        if (!model) return res.redirect(req.header('Referer') || '/');

        req.body.creador = global.user._id;
        switch (req.params.path) {
            case 'personajes':
                req.body.redes_sociales = [];

                if (req.body.twitter) req.body.redes_sociales.push({ red: 'Twitter', url: req.body.twitter });
                if (req.body.facebook) req.body.redes_sociales.push({ red: 'Facebook', url: req.body.facebook });
                if (req.body.instagram) req.body.redes_sociales.push({ red: 'Instagram', url: req.body.instagram });
                if ('pic_base64' in req.body && req.body.pic_base64.length) {
                    let pic = req.body.pic_base64.replace(/^data:image\/png;base64,/, "");
                    let name = uuid() + req.body.ext;
                    let filename = './public/uploads/' + name;
                    
                    fs.writeFileSync(filename, pic, 'base64');

                    req.body.foto_perfil = name;
                }
                
                break;
        }

        model.findOne({ _id: req.params.id }).exec((err, collection) => {
            if (err) {
                req.flash('error', err.message);
                return res.redirect(req.header('Referer') || '/');
            }

            if (collection) {
                switch (req.params.path) {
                    case 'personajes':
                        if ('file' in req &&
                            req.file &&
                            collection.foto_perfil &&
                            req.file.filename != collection.foto_perfil) {
                            // Deletes the old photo:
                            fs.unlinkSync('./public/uploads/' + collection.foto_perfil);
                        }

                        require('./actions/personajes/edit_post')(req, res, collection);
                        return;
                        break;
                }
                // Guardo:
                for (var n in req.body) {
                    if (req.body[n]) collection[n] = req.body[n];
                }

                collection.save((err) => {
                    if (err) {
                        req.flash('error', err.message);
                        return res.redirect(req.header('Referer') || '/');
                    }

                    switch (req.params.path) {
                        case 'ofertas':
                            return res.redirect('/admin/ofertas');
                    }

                    res.redirect('/admin/' + req.params.path + '/' + collection._id);
                });
            }
        });
    });

    route.get('/:path/tree', auth, (req, res) => {
        res.render(req.params.path + '/tree');
    });

    // VIEW
    route.get('/:path/:id', auth, (req, res) => {
        switch (req.params.path) {
            case 'personajes':
                require('./actions/personajes/view_get')(req, res); return;
        }

        let model = global.getModel(req.params.path);
        if (!model) return res.redirect(req.header('Referer') || '/');

        model = model.findOne({ _id: req.params.id });
        model = global.modelPopulate(model, req.params.path);
        model.lean().exec((err, found) => {
            if (err) {
                req.flash('error', err.message);
                return res.redirect(req.header('Referer') || '/');
            }

            if (found) {
                res.render(req.params.path + '/view', found);
            } else {
                res.redirect('/admin/' + req.params.path); // Not found
            }
        });
    });

    route.post('/:path/:id/upload', [auth, upload.single('file')], (req, res) => {
        let model = global.getModel(req.params.path);
        if (!model) return res.redirect(req.header('Referer') || '/');

        model.findOne({ _id: req.params.id }).exec((err, obj) => {
            if (err || !obj) {
                req.flash('error', err.message);
                return res.redirect(req.header('Referer') || '/');
            }

            let file = global.models.Upload({
                model: global.getModelName(req.params.path),
                document: obj._id,
                usuario: global.user._id,
                originalname: req.file.originalname,
                filename: req.file.filename,
                path: req.file.path,
                filesize: req.file.size,
                mimetype: req.file.mimetype
            });

            file.save((err, saved) => {
                if (err) {
                    req.flash('error', err.message);
                    return res.redirect(req.header('Referer') || '/');
                }

                if (!('archivos' in obj)) {
                    obj.archivos = [];
                }

                obj.archivos.push(saved._id);
                obj.save(() => {
                    return res.redirect(req.header('Referer') || '/');
                });
            });
        });
    });

    route.post('/:path/:id/comment', auth, (req, res) => {
        let model = global.getModel(req.params.path);
        if (!model) return res.redirect(req.header('Referer') || '/');
        
        // Dejar comentario a un modelo
        model.findOne({ _id: req.params.id }).exec((err, obj) => {
            if (err || !obj) {
                req.flash('error', err.message);
                return res.redirect(req.header('Referer') || '/');
            }

            let comment = global.models.Comentario({
                usuario: global.user._id,
                model: global.getModelName(req.params.path),
                referencia: obj._id,
                comentario: req.body.comentario
            });

            comment.save((err, saved) => {
                if (!err) {
                    if (!('comentarios' in obj)) {
                        obj.comentarios = [];
                    }

                    obj.comentarios.push(saved._id);
                    obj.save(() => {
                        return res.redirect(req.header('Referer') || '/');
                    });

                } else {
                    req.flash('error', err.message);
                    return res.redirect(req.header('Referer') || '/');
                }
            })
        })
    });

    app.use(function(req, res, next) {
        res.locals.error_message = req.flash('error');
        next();
    });

    app.use('/admin', route);
}

global.modelPopulate = (model, path) => {
    // Populate actions
    switch (path) {
        case 'personajes':
            return model.populate('hojas_de_vida organizacion referido entidad proyectos puesto_votacion referidos')
                .populate({
                    path: 'comentarios',
                    populate: { path: 'usuario' }
                })
                .populate({
                    path: 'archivos',
                    populate: { path: 'usuario' }
                })
                .populate({
                    path: 'ofertas',
                    populate: { path: 'usuario' }
                })
                .populate({
                    path: 'ofertas_usadas',
                    populate: { path: 'usuario' }
                });
            break;
        case 'hojasdevida':
            return model.populate('personaje referido');
            break;
        case 'proyectos':
            return model.populate('organizacion usuario_responsable personaje entidad tareas.responsable tareas.personaje')
                .populate({
                    path: 'comentarios',
                    populate: { path: 'usuario' }
                })
                .populate({
                    path: 'archivos',
                    populate: { path: 'usuario' }
                });
            break;
        case 'ofertas':
            return model.populate('usuario personaje');
            break;
        case 'organizaciones':
            return model.populate('integrantes');
            break;
    }

    return model;
}