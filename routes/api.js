"use strict";
const express     = require('express');
const route       = express.Router();

const _           = require('lodash');
const auth        = require('../middleware/auth');
const moment      = require('moment');
const diacritics  = require('diacritics').remove;

const Antecedentes = require('../utils/antecedentes');
const PuestoVotacion = require('../utils/puesto-votacion');

module.exports = (app, mongoose) => {
  route.post('/organizaciones/:id/add', (req, res) => {
    global.models.Personaje.findOne({_id: req.body._id}).exec((err, personaje) => {
      if(err || !personaje) {
        console.log(req.body.id, err, personaje);
        return res.sendStatus(404);
      }

      if(personaje.organizacion) {
        global.models.Organizacion.findOneAndUpdate(
          { _id: personaje.organizacion },
          { $pull: { integrantes: personaje._id }}
        ).exec((err, organizacion) => {
          global.models.Organizacion.findOneAndUpdate(
            { _id: req.params.id},
            { $addToSet: { integrantes: personaje._id } }
          ).exec((err, organizacion) => {
            if(err) {
              return res.sendStatus(500);
            }

            global.models.Personaje.update(
              {_id: req.body._id},
              {organizacion: mongoose.Types.ObjectId(req.params.id)}
            ).exec((err, personaje) => {
              if(err) return res.sendStatus(500);
              res.sendStatus(200);
            });
          });
        });
      } else {
         global.models.Organizacion.findOneAndUpdate(
            { _id: req.params.id},
            { $addToSet: { integrantes: personaje._id } }
          ).exec((err, organizacion) => {
            if(err) {
              return res.sendStatus(500);
            }

            global.models.Personaje.update(
              {_id: req.body._id},
              {organizacion: mongoose.Types.ObjectId(req.params.id)}
            ).exec((err, personaje) => {
              if(err) return res.sendStatus(500);
              res.sendStatus(200);
            });
          });
      }
    });
  });

  route.get('/personajes/:id/puesto_votacion', (req, res) => {
    global.models.Personaje.findOne({_id: req.params.id}).populate('puesto_votacion').exec((err, personaje) => {
      if(personaje) {
        console.log('Buscando antecedentes');
        Antecedentes(personaje.documento, (details) => {
          if(!details) {
            res.sendStatus(401);
            return;
          }
          console.log('buscando puesto');
          PuestoVotacion(personaje.documento, (puesto) => {
            console.log(personaje.puesto_votacion, puesto, details);
            personaje.antecedentes = details.antecedentes;

            if(typeof personaje.puesto_votacion == 'undefined' || personaje.puesto_votacion.nombre != puesto.nombre) {
              global.models.PuestoVotacion.findOne({ nombre: puesto.nombre }).lean().exec((err, existing_puesto) => {
                if (!existing_puesto) {
                  let nuevo = global.models.PuestoVotacion(puesto);
                      nuevo.save((err, saved) => {
                        if(err) {
                          return res.sendStatus(501);
                        }

                        personaje.puesto_votacion = saved._id;
                        personaje.mesa_votacion = puesto.mesa;
                        personaje.save((err) => {
                          res.sendStatus((err) ? 500 : 200);
                        });
                      });
                } else {
                  personaje.puesto_votacion = existing_puesto._id;
                  personaje.mesa_votacion = puesto.mesa;
                  personaje.save((err) => {
                    res.sendStatus((err) ? 500 : 200);
                  });
                }
              });
            } else {
              personaje.save((err) => {
                res.sendStatus((err) ? 500 : 200);
              });
            }
          });
        });
      } else {
        res.sendStatus(404);
      }
    });
  });

  route.get('/tree/personajes', (req, res) => {
    let model = global.models.Personaje;
    // Root:
    let root = {
      id: '',
      nombre: 'Campaña',
      children: []
    };

    // Busco los children
    model.find(
      {referido: null}, 
      {_id: 1, nombre: 1, referidos: 1}
    ).lean().exec((err, personajes) => {
      if(err) {
        return res.sendStatus(500);
      }

      _.each(personajes, (personaje) => {
        personaje.id = personaje._id;
        personaje.hasChild = ('referidos' in personaje && personaje.referidos.length > 0);

        delete personaje._id;
        if('referidos' in personaje) delete personaje.referidos;

        root.children.push(personaje);
      });

      return res.json(root);
    });
  });

  route.get('/tree/personajes/:id', (req, res) => {
    let model = global.models.Personaje; //global.getModel(req.params.path);
    //if (!model) return res.sendStatus(500);
    let root = {
      result: []
    };

    // Busco los children
    model.find(
      {referido: req.params.id}, 
      {_id: 1, nombre: 1, referidos: 1}
    ).lean().exec((err, personajes) => {
      if(err) {
        return res.sendStatus(500);
      }

      _.each(personajes, (personaje) => {
        personaje.id = personaje._id;
        personaje.hasChild = ('referidos' in personaje && personaje.referidos.length > 0);

        delete personaje._id;
        if('referidos' in personaje) delete personaje.referidos;

        root.result.push(personaje);
      });

      return res.json(root);
    });
  });

  route.post('/personajes/:id/referido', (req, res) => {
    let a = req.params.id;
    let b = req.body.set;

    // Elimino del que me tenga como referido
    global.models.Personaje.update({referidos: a}, {$pull: { referidos: a}}, (err, num) => {
      // Modifico a con referido b
      global.models.Personaje.update({_id: a}, {$set: {referido: b}}, (err, num) => {
        // Agrego al personaje b el referido a
        if(b == '') {
          res.json({updated: num});
        } else {
          global.models.Personaje.update({_id: b}, {$push: {referidos: a}}, (err, num) => {
            res.json({updated: num});
          });
        }
      });
    });
  });

  route.post('/personajes/:id/:field', (req, res) => {
    let valid = [
      'color',
      'agregado_contactos_google',
      'enviar.sms',
      'enviar.llamadas',
      'enviar.email',
      'enviar.correspondencia',
    ];

    if(valid.indexOf(req.params.field) >= 0) {
      let set = {$set: {}};
          set['$set'][req.params.field] = req.body.set;

      global.models.Personaje.update(
        { _id: req.params.id },
        set,
        (err, numAffected) => {
          res.json({updated: numAffected});
        }
      );
    } else {
      res.json({});
    }
  });

  route.post('/:path/delete', (req, res) => {
    let model = global.getModel(req.params.path);
    if (!model) return res.sendStatus(500);

    model.findOne({ _id: req.body._id }).exec((error, record) => {
        if (!error) {
          record.delete(mongoose.Types.ObjectId(global.user._id), (err) => {
              if (err) {
                    res.sendStatus(500);
              } else {
                    res.sendStatus(200);
              }
          });
        } else {
          res.sendStatus(404);
        }
    });
  });

  route.post('/personajes/:id/organizacion', (req, res) => {
    global.models.Personaje.findOne({
      _id: req.params.id
    }).exec((err, personaje) => {
      if(err) {
        return res.json({err});
      }

      let old = personaje.organizacion;
      personaje.organizacion = req.body.organizacion;
      personaje.save((err, saved) => {
        if(err) {
          return res.json({err});
        }

        // Actualizo la organización anterior para sacar al usuario de ahi
        global.models.Organizacion.update(
          {_id: old},
          {$pull: { integrantes: {_id: personaje._id}, representantes: {_id: personaje._id}}},
          {safe: true}, (err) => {
          // Actualizo la organización actual con el usuario
          if(err) {
            return res.json({err});
          }
          
          global.models.Organizacion.findOneAndUpdate(
            {_id: req.body.organizacion}, 
            {$addToSet: {integrantes: personaje._id}},
            (err) => {
            return res.json({err});
          });
        });
      });
    });
  });

  route.post('/task/:id', (req, res) => {
      global.models.Proyecto.update(
        { 'tareas._id': req.params.id },
        { 'tareas.$.finalizado': req.body.checked },
        (err, numAffected) => {
          res.json({updated: numAffected});
        }
      )
  });

  route.post('/tarea/update', (req, res) => {
      global.models.Proyecto.update(
        { 'tareas._id': req.body._id },
        req.body.set,
        (err, numAffected) => {
          res.json({error: err, updated: numAffected});
        }
      )
  });

  route.post('/tarea/comentario', (req, res) => {
    let comment = global.models.Comentario({
      usuario: global.user._id,
      model: 'Tarea',
      referencia: req.body._id,
      comentario: req.body.comentario
    });

    comment.save((err, comment) => {
      if(!err) {
        global.models.Proyecto.update(
          { 'tareas._id': req.body._id },
          { $push: { 'tareas.$.comentarios': comment._id } },
          (err, numAffected) => {
            res.json({error: err, updated: numAffected, _id: comment._id});
          }
        )
      }
    });
  });

  route.post('/tarea/comentario/delete', (req, res) => {
   global.models.Proyecto.update(
      { 'tareas._id': req.body._id },
      { $pull: { 'tareas.$.comentarios': req.body.comentario } },
      (err, numAffected) => {
        if(err == null) {
          global.models.Comentario.remove({_id: req.body.comentario}).exec((err, numAffected) => {
             res.json({error: err, updated: numAffected});
          });
        } else {
           res.json({error: err, updated: numAffected});
        }
      }
    );
  });

  route.get('/document/:id', (req, res) => {
    global.models.Personaje.findOne({documento: req.params.id}).lean().exec((err, personaje) => {
      if(!personaje) {
        Antecedentes(req.params.id, (details) => {
          if(!details) {
            res.json({error: 'El documento no existe.'});
            return;
          }

          // Si el usuario existe, busco el puesto de votación
          PuestoVotacion(req.params.id, (puesto) => {
            res.json({
              nombre: details.name,
              antecedentes: details.antecedentes,
              puesto_votacion: puesto
            });
          });
        });
      } else {
        res.json({error: 'El documento ya se encuentra registrado.'});
      }
    });
  });

  // Devuelve un arreglo de personajes que coinciden con la búsqueda por nombre
  route.get('/search/ofertas', auth, (req, res) => {
    let query = {};

    if(req.query.categoria) {
      query.categoria = new RegExp(req.query.categoria, 'ig');
    }

    if(req.query.buscar) {
      query.$or = [
        { tipo: new RegExp(req.query.buscar, 'ig') },
        { descripcion: new RegExp(req.query.buscar, 'ig') }
      ];
    }

    global.models.Oferta
      .find(query)
      .select('tipo descripcion categoria fecha_fin personaje')
      .populate('personaje')
      .limit(10)
      .lean()
      .exec((err, found) => {
        if(err) throw err;
        res.json(found);
      });
  });

  // Devuelve un arreglo de personajes que coinciden con la búsqueda por nombre
  route.get('/search/:path', auth, (req, res) => {
    let model = global.getModel(req.params.path);
    if(!model) {
      return res.json({results: [], error: 'Not found'});
    }

    let query = {
      nombre: new RegExp(req.query.q, 'ig')
    };

    if(global.getModelName(req.params.path) == 'User') {
      query = {
        name: new RegExp(req.query.q, 'ig')
      };
    }

    if('organizacion' in req.query) {
      query.organizacion = req.query.organizacion;
    }

    model.find(query).limit(10).lean().exec((err, found) => {
      if(err) throw err;
      if(found) {
        let result = [];

        _.each(found, (e) => {
          result.push({id: e._id, text: e.nombre || e.name});
        });

        res.json({results: result});
      } else {
        res.json({results: []});
      }
    });
  });

  route.get('/projectos/tareas', auth, (req, res) => {
    let model = global.models.Proyecto;

    let date = moment(req.query.date);
    let query = {
      'tareas.fecha_fin': {$lt: date}
    };

    model.find(query).populate('tareas.responsable').lean().exec((err, proyectos) => {
      if(err) throw err;

      let tareas = [];
      _.each(proyectos, (proyecto) => {
        _.each(proyecto.tareas, (tarea) => {
          tareas.push({
            title: tarea.responsable.nombre,
            start: tarea.fecha_inicio,
            end: tarea.fecha_fin
          });
        });
      });

      res.json({events: tareas});
    });
  });

  route.get('/seen/:model/:id.png', (req, res) => {
    switch(req.params.model) {
      case 'tarea':
        global.models.Proyecto.update(
          { 'tareas._id': req.params.id },
          { 'tareas.$.visto': true },
          (err, numAffected) => {
            // Marca como visto:
            var img = new Buffer('iVBORw0KGgoAAAANSUhEUgAAAPAAAAE', 'base64');

            res.writeHead(200, {
               'Content-Type': 'image/png',
               'Content-Length': img.length
            });

            res.end(img);
          }
        );
      break;
    }
  })

  app.use('/api', route);
}
