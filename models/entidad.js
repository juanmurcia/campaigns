"use strict";
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const EntidadSchema =  new Schema({
    contacto: {type: String},
    nombre: String,
    direccion: String,
    telefono: String,
    vacantes: [{
      area: String,
      disponible: Number,
      fecha_fin: Date,
    }]
  }, { timestamps: true });

module.exports = (connection) => {
  return mongoose.model('Entidad', EntidadSchema);
}
