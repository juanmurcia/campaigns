"use strict";
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;
const Log = require('./log')();

const HojaDeVidaSchema = new Schema({
    personaje: {type: String, ref: 'Personaje'},
    referido: {type: String, ref: 'Personaje'},
    educacion: [{
      nivel_academico: String,
      detalle: String,
      experiencia_laboral: Number,
    }],
    prioridad: String,
    fecha_entregada: Date,
    entidad: {type: String, ref: 'Entidad'},
    area_entidad: String,
    estado: String,
    cargo_convocado: String,
    funciones_convocado: String,
    fecha_ingreso: Date,
    remuneracion_prevista: Number,
    duracion_contrato: Date,
    comentarios: [{type: String, ref: 'Comentario'}]
  }, { timestamps: true });

HojaDeVidaSchema.pre('save', function(next) {
  if(this.isNew) {
    this.wasNew = true;
  }
  next();
});

HojaDeVidaSchema.post('save', function(doc) {
  let log = new Log({
    model: 'HojaDeVida',
    document: doc._id,
    user: global.user._id,
    type: (doc.wasNew) ? 'INSERT' : 'UPDATE'
  });

  log.save();
});

module.exports = (connection) => {
  return mongoose.model('HojaDeVida', HojaDeVidaSchema);
}
