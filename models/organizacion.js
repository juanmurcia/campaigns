"use strict";
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;
const Log = require('./log')();

const OrganizacionSchema = new Schema({
    nombre: { type: String, required: true },
    representantes: [{ type: ObjectId, ref: 'Personaje', unique: true }],
    integrantes: [{ type: ObjectId, ref: 'Personaje', unique: true }],
    descripcion: String,
    potencial_declarado: Number,
    potencial_esperado: Number,
}, { timestamps: true });

OrganizacionSchema.index({
    nombre: 'text',
    descripcion: 'text'
});

OrganizacionSchema.pre('save', function(next) {
    if (this.isNew) {
        this.wasNew = true;
    }
    next();
});

OrganizacionSchema.post('save', function(doc) {
    let log = new Log({
        model: 'Organizacion',
        document: doc._id,
        user: global.user._id,
        type: (doc.wasNew) ? 'INSERT' : 'UPDATE'
    });

    log.save();
});

module.exports = () => {
    return mongoose.model('Organizacion', OrganizacionSchema);
}