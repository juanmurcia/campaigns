"use strict";
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

module.exports = () => {
  return mongoose.model('PuestoVotacion', new Schema({
    nombre: {type: String, index: true, unique: true},
    departamento: String,
    municipio: String,
    direccion: String,
    latitud: Number,
    longitud: Number
  }, { timestamps: true }));
}
