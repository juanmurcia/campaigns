"use strict";
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const FormacionAcademicaSchema = new Schema({
  nombre: String
}, { timestamps: true });


module.exports = () => {
  return mongoose.model('FormacionAcademica', FormacionAcademicaSchema);
}
