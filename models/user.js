"use strict";
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const UserSchema = new Schema({
  name: String,
  email: {type: String, unique: true},
  password: String,
  roles: {type: String, ref: 'Roles'},
  active: {type: Boolean, default: true},
  profile_pic: {type: String, default: '/portraits/default.png'},
  recover_string: String
}, { timestamps: true });

module.exports = () => {
  return mongoose.model('User', UserSchema);
};
