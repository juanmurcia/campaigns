"use strict";
const mongoose = require('mongoose');
const _ = require('lodash');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;
const Log = require('./log')();
const mongoose_delete = require('mongoose-delete');

module.exports = (connection) => {
    let PersonajeSchema = new Schema({
        activo: { type: Boolean, default: true },
        foto_perfil: String,
        descripcion: String,
        agregado_contactos_google: { type: Boolean, default: false },
        organizacion: { type: ObjectId, ref: 'Organizacion', set: global.ignoreEmpty },
        referido: { type: ObjectId, ref: 'Personaje', set: global.ignoreEmpty },
        creador: { type: ObjectId, ref: 'Personaje', set: global.ignoreEmpty },
        nombre: { type: String, index: true, required: true },
        sexo: String,
        telefono_movil: String,
        telefono_movil_alterno: String,
        telefono_fijo: {
            indicativo: Number,
            numero: Number
        },
        enviar: {
            email: { type: Boolean, default: true },
            sms: { type: Boolean, default: true },
            llamadas: { type: Boolean, default: true },
            correspondencia: { type: Boolean, default: true },
        },
        color: { type: String, default: "default" },
        email_personal: String,
        puesto_votacion: { type: ObjectId, ref: 'PuestoVotacion', set: global.ignoreEmpty },
        mesa_votacion: Number,
        documento: { type: Number, index: true, unique: true, required: true },
        fecha_nacimiento: { type: Date, set: global.ignoreEmpty },
        direccion_residencia: String,
        localidad: { type: ObjectId, ref: 'Localidad', set: global.ignoreEmpty },
        formacion_academica: String,
        nivel_academico: String,

        formacion: [{
            nivel_academico: String,
            descripcion: String
        }],
        pagina_web: String,
        ocupacion_laboral: String,
        redes_sociales: [{ red: String, url: String }],
        edad: Number,
        pariente: [{
            personaje: { type: ObjectId, ref: 'Personaje' },
            edad: Number,
            parentesco: String
        }],
        comentarios: [{ type: ObjectId, ref: 'Comentario' }],
        etiquetas: [String],

        // Electoral
        potencial_declarado: Number,
        potencial_esperado: Number,
        // porencial_registrado -> basado en los personajes
        potencial_urnas: [{
            departamento: String,
            municipio: String,
            fecha: Date,
            votos: Number,
            descripcion: String
        }],
        potencial_otras_regiones: [{
            departamento: String,
            municipio: String,
            votos: Number,
            descripcion: String
        }],

        antecedentes_judiciales: String,
        antecedentes_politicos: [{ type: ObjectId, ref: 'Comentario' }],
        carnetizado: { type: Boolean, default: false },

        entidad: { type: ObjectId, ref: 'Entidad' },
        cargo: String,
        cedula_inscrita: { type: Boolean, default: false },
        proyectos: [{ type: ObjectId, ref: 'Proyecto' }],
        referidos: [{ type: ObjectId, ref: 'Personaje' }],
        hojas_de_vida: [{ type: ObjectId, ref: 'HojaDeVida' }],
        ofertas: [{ type: ObjectId, ref: 'Oferta' }],
        ofertas_usadas: [{ type: ObjectId, ref: 'Oferta' }],
        archivos: [{ type: ObjectId, ref: 'Upload' }]
    }, { timestamps: true });

    PersonajeSchema.index({
        nombre: 'text',
        descripcion: 'text',
        ocupacion_laboral: 'text',
        telefono_movil: 'text',
        telefono_movil_alterno: 'text',
        cargo: 'text',
        'formacion.descripcion': 'text'
    });

    PersonajeSchema.plugin(mongoose_delete, { deletedAt: true, overrideMethods: ['count', 'find'], deletedBy: true });

    PersonajeSchema.pre('save', function(next) {
        if (this.isNew) {
            this.wasNew = true;
        }

        if (_.size(this.proyectos) == 0) {
            delete this.proyectos;
        }

        next();
    });

    PersonajeSchema.post('save', function(doc) {
        let log = new Log({
            model: 'Personaje',
            document: doc._id,
            user: global.user._id,
            type: (doc.wasNew) ? 'INSERT' : 'UPDATE'
        });

        log.save();
    });

    return mongoose.model('Personaje', PersonajeSchema);
};