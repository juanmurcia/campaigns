"use strict";
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;
const Log = require('./log')();
const mongoose_delete = require('mongoose-delete');

const ProyectoSchema = new Schema({
    titulo: String,
    creador: { type: ObjectId, ref: 'User' },
    usuario_responsable: [{ type: ObjectId, ref: 'User' }],
    organizacion: { type: ObjectId, ref: 'Organizacion' },
    personaje: { type: ObjectId, ref: 'Personaje' },
    objetivo: String,
    descripcion: String,
    color: String,
    tipo: String,
    tareas: [{
        titulo: String,
        descripcion: String,
        finalizado: { type: Boolean, default: false },
        notificar: Date,
        fecha_creacion: Date,
        fecha_finalizado: Date,
        fecha_inicio: Date, // Calendario
        fecha_fin: Date, // Calendario
        creado: { type: ObjectId, ref: 'User' },
        responsable: { type: ObjectId, ref: 'User' },
        personaje: { type: ObjectId, ref: 'Personaje' },
        visto: { type: Boolean, default: false }, // si el responsable vio la tarea
        comentarios: [{ type: ObjectId, ref: 'Comentario' }],
        categoria: String,
        estado: String
    }],
    finalizado: { type: Boolean, default: false },
    detalle: String,
    egresos: Number,
    comentarios: [{ type: ObjectId, ref: 'Comentario' }],
    archivos: [{ type: ObjectId, ref: 'Upload' }],
    activo: { type: Boolean, default: true }
}, { timestamps: true });

ProyectoSchema.index({
    titulo: 'text',
    objetivo: 'text',
    descripcion: 'text',
    'tareas.titulo': 'text',
    'tareas.descripcion': 'text'
});

ProyectoSchema.plugin(mongoose_delete, { deletedAt: true, overrideMethods: ['count', 'find'], deletedBy: true });

ProyectoSchema.pre('save', function(next) {
    if (this.isNew) {
        this.wasNew = true;
    }
    next();
});

ProyectoSchema.post('save', function(doc) {
    let log = new Log({
        model: 'Proyecto',
        document: doc._id,
        user: global.user._id,
        type: (doc.wasNew) ? 'INSERT' : 'UPDATE'
    })

    log.save();
});


module.exports = () => {
    return mongoose.model('Proyecto', ProyectoSchema)
};