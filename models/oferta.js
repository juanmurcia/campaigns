"use strict";
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

module.exports = (connection) => {
  const OfertaSchema =  new Schema({
      descripcion: String,
      fecha_fin: Date,
      categoria: String,
      activo: {type: Boolean, default: true},
      tipo: String,
      personaje: {type: ObjectId, ref: 'Personaje'},
      usuario: {type: ObjectId, ref: 'User'}
    }, { timestamps: true });

  OfertaSchema.index({
    descripcion: 'text',
    tipo: 'text'
  });

  return mongoose.model('Oferta', OfertaSchema);
}
