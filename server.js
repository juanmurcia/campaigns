"use strict";
const PORT = process.env.PORT || 80;
/* Módulos */
const express = require('express');
const cors = require('cors');
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser');
const exphbs = require('express-hbs');
const helmet = require('helmet');
const uuid = require('uuid/v4');
const flash = require('connect-flash');
const session = require('express-session');

const request = require('request');
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGODB_URI || '127.0.0.1:27017/campaign'); // connect to database

const connection = mongoose.createConnection(process.env.MONGODB_URI || '127.0.0.1:27017');


// Mongoose uses this for ObjectId null references when saving
global.ignoreEmpty = (val) => {
    return ("" === val) ? undefined : val;
}

// Models
global.models = {};
global.models.Comentario = require('./models/comentario')(connection);
global.models.User = require('./models/user')(connection);
global.models.Personaje = require('./models/personaje')(connection);
global.models.Organizacion = require('./models/organizacion')(connection);
global.models.HojaDeVida = require('./models/hoja_de_vida')(connection);
global.models.Entidad = require('./models/entidad')(connection);
global.models.Localidad = require('./models/localidad')(connection);
global.models.Proyecto = require('./models/proyecto')(connection);
global.models.Etiqueta = require('./models/etiqueta')(connection);
global.models.PuestoVotacion = require('./models/puesto_votacion')(connection);
global.models.FormacionAcademica = require('./models/formacion_academica')(connection);
global.models.Log = require('./models/log')(connection);
global.models.Upload = require('./models/upload')(connection);
global.models.Oferta = require('./models/oferta')(connection);

// Settings for JWT
global.user = {};
global.secret = '!!<_^vcMIUHLiIxZXj8oQmuJ^!7VAFMvscFG0HyRDfbGszPyWyika1';

global.getModel = (path) => {
    switch (path) {
        case 'personajes':
            return global.models.Personaje;
            break;
        case 'organizaciones':
            return global.models.Organizacion;
            break;
        case 'hojasdevida':
            return global.models.HojaDeVida;
            break;
        case 'entidades':
            return global.models.Entidad;
            break;
        case 'localidades':
            return global.models.Localidad;
            break;
        case 'proyectos':
            return global.models.Proyecto;
            break;
        case 'usuarios':
            return global.models.User;
            break;
        case 'ofertas':
            return global.models.Oferta;
            break;
    }

    return false;
}

global.getModelName = (path) => {
    switch (path) {
        case 'personajes':
            return 'Personaje'
            break;
        case 'organizaciones':
            return 'Organizacion';
            break;
        case 'hojasdevida':
            return 'HojaDeVida';
            break;
        case 'entidades':
            return 'Entidad';
            break;
        case 'localidades':
            return 'Localidad';
            break;
        case 'proyectos':
            return 'Proyecto';
            break;
        case 'usuarios':
            return 'User';
            break;
        case 'ofertas':
            return 'Oferta';
            break;
    }

    return false;
}

global.getModelPath = (name) => {
    switch (name) {
        case 'Personaje':
            return 'personajes'
            break;
        case 'Organizacion':
            return 'organizaciones';
            break;
        case 'HojaDeVida':
            return 'hojasdevida';
            break;
        case 'Entidad':
            return 'entidades';
            break;
        case 'Localidad':
            return 'localidades';
            break;
        case 'Proyecto':
            return 'proyectos';
            break;
        case 'User':
            return 'usuarios';
            break;
        case 'Oferta':
            return 'ofertas';
            break;
    }

    return false;
}

/* Server */
let app = express();

let handlebars = exphbs.express4({
    partialsDir: __dirname + '/views/partials',
    defaultLayout: __dirname + '/views/layouts/main',
    extname: '.handlebars'
});

require('./utils/handlebar-helpers.js')(exphbs);

app.engine('handlebars', handlebars);
app.set('view engine', 'handlebars');
app.set('views', __dirname + '/views');

app.use(helmet());
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true, limit: '5mb' }));
app.use(bodyParser.json({limit: '5mb'}));
app.use(express.static('public'));

app.use(cookieParser());
app.use(session({
    secret: 'RCCSCJ629zv7oy#2pRhac054Xq6m1z@f6%3vb',
    resave: false,
    saveUninitialized: true
}));

app.use(flash());
app.use(require('express-json-2-csv')());

app.get('/', function(req, res) {
    res.redirect('/admin/'); // POR AHORA ENVIAMOS AL ADMIN SIEMPRE
});

require('./routes/admin.js')(app, mongoose, connection);
require('./routes/api.js')(app, mongoose, connection);

app.listen(PORT);
console.log('Server running, go to http://localhost:' + PORT);

// Funcion para envio de mails:

const sendinblue = require('sendinblue-api');
const mail = new sendinblue({ "apiKey": 'K70dxg53vfG2PHhp', "timeout": 5000 });
global.sendMail = (name, email, subject, render, content, callback) => {
    let defaults = {
        subject: subject,
        from: ['info@bogotasoyyo.co', 'Información Importante']
    };

    content.layout = 'layouts/email';
    app.render('emails/' + render, content, function(err, html) {
        let send_email = {};
        send_email[email] = name;

        mail.send_email({
            to: send_email,
            from: defaults.from,
            subject: defaults.subject,
            html: html
        }, function(res) {
            console.log('Respuesta SendInBlue:', arguments);

            try {
                callback();
            } catch (e) { console.log(e); }
        });
    });
};