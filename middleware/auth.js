// This middleware checks if the user has a jwt token and if its valid.
const jwt = require('jsonwebtoken');
const secret = '^vcMIUHLiIxZXj8oQmuJ^!7VAFMvscFG0HyRDfbGszPyWyika1';

function jwtCheck(req, res, next) {
  var token = req.cookies.jwt_auth || req.headers['x-access-token'];

  if (token) {
    jwt.verify(token, global.secret, function(err, decoded) {
      if (err) {
        console.log('middleware/jwt.js', err);
        return res.redirect('/admin/login');
      } else {
        global.user = decoded._doc;
        req.decoded = decoded._doc;
        next();
      }
    });
  } else {
    return res.redirect('/admin/login');
  }
}

module.exports = jwtCheck;
